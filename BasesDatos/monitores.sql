use FUCS;
go
declare
	@number integer,
	@ip varchar(10);
begin
	set @number = 1
	while 100 > @number 
	begin
		set @ip = '196.76.0.'
		insert into monitor(ip) values (@ip +cast( @number as varchar(3)))
		set @number=@number +1
	end;
end
go