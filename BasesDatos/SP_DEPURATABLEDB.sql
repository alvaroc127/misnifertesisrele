USE FUCS
GO
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'SP_DEPURATABLEDB' AND type = 'P')
	DROP PROCEDURE dbo.SP_DEPURATABLEDB;
go
CREATE PROCEDURE SP_DEPURATABLEDB
@codexit int  OUTPUT,
@mesaje varchar output
AS 
BEGIN 
	DECLARE 
		@vrl_regtem int,
		@vrl_regECG int,
		@vrl_regFreRe int,
		@vrl_regAlrm int,
		@vrl_regSenalRA int,
		@vrl_SPO2 int
	
	SELECT  @vrl_regtem = 0
	SELECT  @vrl_regECG = 0
	SET  @vrl_regFreRe = 0
	SET   @vrl_regAlrm = 0
	SET   @vrl_regSenalRA = 0
	SET  @vrl_SPO2 = 0
	
	SELECT @vrl_regtem=COUNT(HoraSenal)   FROM dbo.Temperatura
	SELECT @vrl_regECG=COUNT(HoraSenal)   FROM dbo.ECG
	SELECT @vrl_regFreRe=COUNT(HoraSenal)  FROM dbo.Frec_Respiratoria
	SELECT @vrl_regAlrm=COUNT(HoraAlarma)  FROM dbo.Alarm
	SELECT @vrl_regSenalRA=COUNT(HoraSenal) FROM dbo.Senal_Roja_Amarilla
	SELECT @vrl_SPO2=COUNT(HoraSenal)   FROM dbo.SPO2
BEGIN TRY
	IF(@vrl_regtem > 10)
		DELETE FROM dbo.Temperatura 
		WHERE HoraSenal  IN ( SELECT TOP 10 HoraSenal FROM dbo.Temperatura 
		ORDER BY HoraSenal ASC)
	
	IF(@vrl_regECG > 10)
		DELETE FROM dbo.ECG 
		WHERE HoraSenal  IN ( SELECT TOP 10 HoraSenal FROM dbo.ECG 
		ORDER BY HoraSenal ASC)

	IF(@vrl_regFreRe > 10)
		DELETE FROM dbo.Frec_Respiratoria 
		WHERE HoraSenal  IN 
			( SELECT TOP 10 HoraSenal FROM dbo.Frec_Respiratoria 
		ORDER BY HoraSenal ASC)


	IF(@vrl_regAlrm > 10)
		DELETE  FROM dbo.Alarm
			WHERE HoraAlarma 
			IN (
			SELECT TOP 10 HoraAlarma FROM Alarm a2 
			GROUP BY HoraAlarma
			ORDER BY HoraAlarma ASC)

	IF(@vrl_regSenalRA > 10)
		DELETE FROM dbo.Senal_Roja_Amarilla 
		WHERE HoraSenal  IN 
		( 
		SELECT TOP 10 HoraSenal FROM dbo.Senal_Roja_Amarilla 
		GROUP BY HoraSenal
		ORDER BY HoraSenal ASC
		)

	IF(@vrl_SPO2 > 10)
		DELETE FROM dbo.SPO2
		WHERE HoraSenal  IN 
		( 
		SELECT TOP 10 HoraSenal FROM dbo.SPO2
		ORDER BY HoraSenal ASC
		)
		
	SELECT @codexit = 0
	SELECT @mesaje='depuracion exitosa'
	SELECT @codexit,@mesaje 
	RETURN 
END TRY

BEGIN CATCH
SELECT @codexit = ERROR_NUMBER()
SELECT @mesaje=ERROR_MESSAGE()
	SELECT @codexit,@mesaje
RETURN 
END CATCH
END;
go