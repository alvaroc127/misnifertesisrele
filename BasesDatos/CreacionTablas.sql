CREATE TABLE Monitor(
	ID INT IDENTITY(1,1),
	IP varchar(100),
	Num_Cama INT,
	FechaRegistro datetime,
	PRIMARY KEY (ID)
);
CREATE TABLE Temperatura(
	HoraSenal datetime,
	T1 FLOAT,
	T2 FLOAT,
	TD FLOAT,  
	ID INT,
	PRIMARY key(HoraSenal),
	CONSTRAINT fk_mSonTemp
	FOREIGN KEY (ID) REFERENCES Monitor(ID) 
);
CREATE TABLE ECG
(
	ID INT,
	HoraSenal datetime,
	aVR float,
	aVl float,
	Frec_Cardi float,
	I float,
	II float,
	III float,
	V float,
	aVF float,
	CVPs float,
	ECG1 varbinary(257),
	ECG2 varbinary(257),
	ECG3 varbinary(257),
	PRIMARY KEY(HoraSenal),
	CONSTRAINT fk_monECG
	FOREIGN KEY (ID) REFERENCES Monitor(ID) 
);
CREATE TABLE Frec_Respiratoria(
	HoraSenal datetime,
	impedancia float,
	ID INT,
	Senal varbinary(257),
	PRIMARY KEY (HoraSenal),
	CONSTRAINT fk_monFreResp
	FOREIGN KEY (ID) REFERENCES Monitor(ID)
);
CREATE TABLE ALARMABDID(
	 ID  INT  IDENTITY(1,1),
 	 Descripcion varchar(255),
 	 severidad INT,
 	 PRIMARY KEY (ID)
);
create TABLE  Alarm(
	ID INT, 
	HoraAlarma datetime,
	severidad integer,
	Descripcion varchar(255),
	ECG1 varbinary(257),
	ECG2 varbinary(257),
	ECG3 varbinary(257),
	aVR float,
	aVL FLOAT,
	Frec_Cardi float,
	I float,
	II float,
	III float,
	V float,
	aVF float,
	CVPs float,
	CONSTRAINT fk_monAlarm
	FOREIGN KEY (ID) REFERENCES Monitor(ID),
	PRIMARY KEY (HoraAlarma,Descripcion,severidad)
);
CREATE TABLE Senal_Roja_Amarilla(
	TipoSenal int,
	Maximo float,
	minimo float,
	parentesis float,
	Senal varbinary(257),
	ID INT, 
	HoraSenal datetime, 
	PRIMARY KEY(HoraSenal,TipoSenal),
	CONSTRAINT fk_monSRA
	FOREIGN KEY (ID)
	REFERENCES Monitor(ID)
);
CREATE TABLE SPO2(
	frecuencia float,
	id INT,
	HoraSenal datetime, 
	desconocido float,
	SenalSPO2 varbinary(257),
	PRIMARY KEY(HoraSenal),
	CONSTRAINT fk_mon
	FOREIGN KEY (id)
	REFERENCES Monitor(ID)
);

CREATE TABLE TIPO_DOC
(
	ID_DOC  INT  IDENTITY(1,1),
	STATUS  SMALLINT,
	DESCRIP varchar(100),
	PRIMARY KEY (ID_DOC)
);

CREATE TABLE USUARIO
(
	ID_USU INT IDENTITY(1,1) UNIQUE,
	TIPO_DOC INT,
	EMAIL varchar(255),
	NUMERO_DOC varchar(20),
	GENERO SMALLINT,
	FECHA_NACIMIENTO DATE,
	DIRECCION VARCHAR(255),
	NUMEROCEL varchar(25),
	password varchar(1024),
	STATUS  SMALLINT, 
	DATE_CREATE datetime DEFAULT getdate(),
	date_update datetime,
	APPMOVIL varchar(1) DEFAULT 'N',
	ALERTAS varchar(1) DEFAULT 'N',
	PRIMARY KEY(TIPO_DOC,NUMERO_DOC),
	CONSTRAINT fk_tipodoc
	FOREIGN KEY (TIPO_DOC) 
	REFERENCES TIPO_DOC(ID_DOC)
);

CREATE TABLE USUARIOXMONITOR --solo registro en esta tabla  cuando el usuraio se registra como paciente
(
	ID_USU INT,
	ID INT,
	DATE_CREATE datetime DEFAULT getdate(),
	CONSTRAINT fk_monusu
	FOREIGN KEY(ID)
	REFERENCES Monitor(ID),
	CONSTRAINT fk_idusu
	FOREIGN KEY(ID_USU)
	REFERENCES USUARIO(ID_USU)
);

CREATE TABLE PERMISOS
(
	ID_PER INT IDENTITY(1,1),
	STATUS  SMALLINT,
	NOMBRE VARCHAR(20)
	PRIMARY KEY (ID_PER)
);


CREATE TABLE ROL
(
	ID_ROL INT IDENTITY(1,1),
	STATUS  SMALLINT,
	NOMBRE  VARCHAR(20)
	PRIMARY KEY (ID_ROL)
);

CREATE TABLE ROLXPERMISOS
(
	ID_ROL INT,
	ID_PER INT,
	DATE_CREATE datetime DEFAULT getdate(),
	DATE_UPDATE datetime,
	CONSTRAINT fk_rolper
	FOREIGN KEY (ID_ROL)
	REFERENCES ROL(ID_ROL),
	CONSTRAINT fk_per
	FOREIGN KEY (ID_PER)
	REFERENCES PERMISOS(ID_PER)
);


CREATE TABLE USUARIOXROL 
(
	ID_ROL INT,
	ID_USU INT,
	DATE_CREATE datetime DEFAULT getdate(),
	DATE_UPDATE datetime,
	CONSTRAINT fk_idusurol
	FOREIGN KEY(ID_USU)
	REFERENCES USUARIO(ID_USU),
	
	CONSTRAINT fk_rolusu
	FOREIGN KEY (ID_ROL)
	REFERENCES ROL(ID_ROL)
);


CREATE TABLE AUDITORIA 
(
	audind INT	IDENTITY(1,1),
	audisource varchar(150),
	audidate datetime,
	audipath varchar(150),
	event1 varchar(300),
	event2 varchar(400),
	eventus varchar(300),
	audicreate datetime DEFAULT getDate()
	PRIMARY KEY (audind)
);


/*
CREATE TABLE PACIENTE(
	CEDULA INT,
	UNIDAD VARCHAR(255),
	NUMERO_CAMA INT,
	ID_CAMA INT,
	NOMBRE VARCHAR(255),
	GENERO SMALLINT,
	TIPO_PACIENTE INT,
	FECHA_NACIMIENTO DATE,
	DIRECCION VARCHAR(255),
	PRIMARY KEY(CEDULA)
);

CREATE TABLE HABITACION(
	ID INT,
	CEDULAPACIENTE INT,
	CONSTRAINT FK_CEDULA
	FOREIGN KEY(CEDULAPACIENTE)
	REFERENCES PACIENTE(CEDULA),
	CONSTRAINT FK_ID
	FOREIGN KEY(ID)
	REFERENCES MONITOR(ID)
);
*/