
#include <iostream>
#include <vector>
#include <tins/tins.h>
#include <process.h>
#include <log4z.h>
#include "..\Include\CapturaDeRed.h"



CapturaDeRed ca;


unsigned int startCapture(void  *args);
unsigned int classifiCapture(void *data);
unsigned int  sendPack(void *data);




//========================================================================
int main( ){

	zsummer::log4z::ILog4zManager::GetInstance()->Start();

	HANDLE multhread[2];

	multhread[0] = (HANDLE)_beginthreadex(0, 0, startCapture, 0, 0, 0);
	 
	multhread[1] = (HANDLE)_beginthreadex(0, 0, classifiCapture, 0, 0, 0);

	WaitForMultipleObjects(2, multhread,TRUE, INFINITE);
	
	for(int i=0;i<2;i++)
	CloseHandle(multhread[i]);
	
	//startHAND = (HANDLE)CreateThread(0, 0, startCapture, 0, 0,idthread);
	//CloseHandle(syncmutex);
	return 0;
};


/// <summary>
/// this is at the first Thread.
/// </summary>
/// <param name="data">The data.</param>
/// <returns></returns>
unsigned int startCapture(void * args)
{
	try {
		ca.startCapture();
	}
	catch (const std::exception &e)
	{
		LOGE("Se genero un error en la captura de paquete el programase cierra "<<e.what());
	}
	return 0;
}


/// <summary>
/// Classifis the capture. this is a second thread
/// </summary>
/// <param name="data">The data.</param>
/// <returns></returns>
unsigned int classifiCapture(void *data)
{
	try {
		ca.clasifiData();
	}
	catch (std::exception &e) 
	{
		LOGE("--EEROR EN LA CLASIFICACION, good bay --"<<e.what());
	}
	return 0;
}


/// <summary>
/// Sends the pack. this is a three thread
/// </summary>
/// <param name="data">The data.</param>
/// <returns></returns>
unsigned int sendPack(void *data)
{

	return 0;
}
