#include "..\Include\CapturaDeRed.h"

//#include<crtdbg.h>


using namespace Tins;
using namespace std;
using namespace zsummer::log4z;



/// <summary>
/// This Method capture packet TCP of network and send from claisify
/// </summary>
//unsigned __stdcall CapturarPacket1(void *);
//struct MyData *dta = NULL;

CapturaDeRed::CapturaDeRed() 
{
	
};




CapturaDeRed::~CapturaDeRed()
{
};

/*
sacar carga util empezar mecanismos de clasificacion.
*/
string CapturaDeRed::clasificarPacket(const std::vector<uint8_t> & carga, int pos) {
	string cab;
	stringstream ss;
	if (carga.empty() == false && carga.size() > 4 && pos + 1 < carga.size()) {
		for (int i = 0; i < 6 && i<carga.size()&& pos < carga.size(); i++) {
			ss << std::hex <<(int)carga.at(pos++);
		}
		cab = ss.str();
		ss.clear();
	}
	return cab;
}

void CapturaDeRed::CapturarPacket1() {
	try {
		Tins::Sniffer snfi(configCapture().name(), configSniffer());
		DWORD synmutex;
		int si=0;
	do {
		try {
			synmutex = WaitForSingleObject(this->syncmutex, INFINITE);
			switch (synmutex)
			{
			case WAIT_OBJECT_0:

				this->storePack.push_back(snfi.next_packet());
				si = this->storePack.size();
				//LOGD("[CapturaDeRedd] se aumenta el tama�o del arreglo  " << si);
				//terminar proceso de insertar			
				if (ReleaseMutex(this->syncmutex) != 0)
				{

				}

				break;

			case WAIT_ABANDONED:
				LOGW("[CapturaDeRedd] hilo captura abandonado -el program se cerrara");
				return;
				break;

			}
		}
		catch (pdu_not_found) {
			LOGE(" Error en el PDU ");
		}
		catch (exception_base)
		{
			LOGE("--Error desconocido--");
		}
	  } while (true);
	}
	catch (Tins::pcap_error  &e) {
		LOGE("--Salida  error inesperado-- valide los logs" << e.what());
		throw e;
	}
	catch (exception &e) 
	{
	LOGE("--nO FUE POSIBLE DETECTAR LA INTERFAZ o algo inesperado a pasado" << e.what());
	LOGE("--el programa se cerrara valide los logs" << e.what());
	throw e;
	}

}


void CapturaDeRed::clasifiData() //falta un control de exc
{
	int pos = 0, posAux = 0;
	bool band = true;
	bool contin = true;
	DWORD synmutex;
	do {
		try {
			// falta el control del hilo al objeto store
			synmutex = WaitForSingleObject(this->syncmutex, INFINITE);
			switch (synmutex)
			{
			case WAIT_OBJECT_0:
				try
				{
					if (!this->storePack.empty())
					{
						Packet pack = this->storePack.front();
						if (pack.pdu()->find_pdu<IP>() && pack.pdu()->find_pdu<TCP>() && pack.pdu()->find_pdu<RawPDU>())
						{
							IP ip = pack.pdu()->rfind_pdu<IP>();
							LOGD("[CapturadeRed ]se recibio ip" << ip.src_addr().to_string());
							//std::cout << "se recibio ip" << ip.src_addr().to_string() << std::endl;
							TCP tcp = pack.pdu()->rfind_pdu<TCP>();
							LOGD("[CapturadeRed ]se recibio tcp" << ip.src_addr().to_string());
							//std::cout << "se recibio tcp" << ip.src_addr().to_string() << std::endl;
							RawPDU raw = tcp.rfind_pdu<RawPDU>();
							//std::cout << " inicio el raw " << ip.src_addr().to_string() << std::endl;
							LOGD("[CapturadeRed ]se recibio raw" << ip.src_addr().to_string());
							do {//Logica de clasificacion
								posAux = pos;
								pos = confHead(clasificarPacket(raw.payload(), pos), ip.src_addr().to_string(), raw.payload(), pos);

								if (pos == posAux && this->wait == true) {

									pos = searchHead(raw.payload(), pos);

									if (pos == posAux) {
										band = false;
									}
								}
							} while (pos < raw.payload().size() && true == this->wait && true == band);
							pos = 0;
							wait = true;
							band = true;
							this->storePack.erase(this->storePack.begin());
						}
						else {
							this->storePack.erase(this->storePack.begin());
						}
					}
					//este representa un hilo capturrar y convertir een el payload 
					//la entidad se almacena 			
				}
				catch (pdu_not_found)
				{
					this->storePack.erase(this->storePack.begin());
					LOGW(" Error en el PDU ");
				}

				if (ReleaseMutex(this->syncmutex) != 0)
				{

				}
				break;

			case WAIT_ABANDONED:

				return;
				break;
			}
		}
		catch (exception &ex) 
		{
			LOGE(" Error en la clasificacion segenera una salida del programa  " <<ex.what());
			contin = false;
			throw ex;
		}
	} while (contin);
}




Tins::NetworkInterface  CapturaDeRed::configCapture() {
	NetworkInterface a;
	LOGI("Inicio normal de la aplicacion--OK--");
	/*
		vector<NetworkInterface> infaces = NetworkInterface::all();
		a = infaces.at(0);
		infaces.clear();
	*/try {
		a = NetworkInterface::default_interface();
		if (a.is_up())
		{
			LOGI("[CapturaDeRedd]--Inicia la captura por la tarjeta:" << a.name());
			LOGI(" [CapturaDeRedd]--La Direccion logica es---: "<< a.ipv4_address().to_string());
		}

		if (!a.is_up())
		{
			LOGW(" [CapturaDeRedd] Se espera por el inicio de la tarjeta en linea, "<< a.name());
		}
	}
	catch (const std::exception &e) 
	{
		LOGE("[CapturaDeRedd] error en el inicio , "<<e.what());
		throw e;
	}
	
	return a;
}


 Tins::SnifferConfiguration  CapturaDeRed::configSniffer() {
	SnifferConfiguration sc;
	//sc.set_rfmon(true);
	sc.set_promisc_mode(true);
	//set_snap_len(1024*1024);
	//sc.set_buffer_size((1024 * 1024) * 2);
	sc.set_filter("tcp and ip");
	sc.set_immediate_mode(true);
	return sc;
}


void CapturaDeRed::startCapture() {
	this->syncmutex = CreateMutex(NULL,FALSE,NULL);
	LOGI("[CapturaDeRedd] Se INICIA EL HILO CAPTURA "); 
	try {
		LOGI("--Se cargar el archivo de propiedades----")
			ga.loadPropertis();
			beanC.loadFile();
			beanC.setInterface();
		LOGI("--Fin de carga de propieades/Se solicita la propiead--");

		if (NULL == this->syncmutex)
		{
			//std::cout << "Un error iniciando la aplicacion" << std::endl;
			LOGE("[CapturaDeRedd] Se Genero un error en la creacion del mutex");
			return;
		}
		CapturarPacket1();
	}
	catch (const exception &e) {
		LOGE("-001-Se presento un erro en la carga de propieades--");
		throw e;
	}

}



int CapturaDeRed::confHead(const std::string &head, const std::string &ip,const std::vector<uint8_t>  & datas , int pos) {
	SubTramaECG * sue = NULL;
	if (head == "1509d00") {

		if(this->dat_time.empty()==false){
			
		MindrayPacket mp;
		mp.setFuente(ip);
		mp.setDtaTime(this->dat_time);
		HeaderTram he = mp.getHead();
		he.loadHead(datas, pos);
		if (he.sizePacket() <= (datas.size() - pos) ) {
			mp.setHead(he);
			pos=mp.clasifyData(datas, pos);
			this->wait = true;
			//std::cout << "la fuente que se busca" + head << std::endl;
			if (guardarMP(mp)) {

			}
		}
		else {
			setVector(datas);
			this->mp = mp;
			///std::cout << "! valor" << this->mp.getSubTra().size() << std::endl;
			this->wait = false;
			posG = pos;
			}
		}
		else {
		}
	}
	else if (head == "150cc00") {
		//std::cout << "entre con el head" + head << std::endl;
		this->dat_time.swap(captDta_time());
		MindrayParametros mp1;
		mp1.setFuente(ip);
		mp1.setDtaTime(dat_time);
		HeaderTram he = mp1.getHead();
		he.loadHead(datas, pos);
		if (datas.size() >= he.sizePacket()) {
			//std::cout << "entre  tama�o del paquete" << std::endl;
			mp1.setHead(he);
			pos=mp1.clasifyData(datas, pos);
			this->wait = true;
			for (int i = 0; i < mp1.getSubTra().size() && sue == NULL; i++) {
				sue = dynamic_cast<SubTramaECG*> (mp1.getSubTra().at(i));
				if (sue != NULL) {
					//std::cout << "entre antes de guardar" << std::endl;
					co.setDate(dat_time);
					co.setIp(ip);
					if (ga.searchIp(ip)) {beanC.setgGa(&ga) ; beanC.createMsgHl7(ip, capDta_timeforHl7()); almacenDB(); }//[ALMACEN ANTES DE ALMACENAR LEER LOS ARCHIVOS YGENERAPARQUETES]
					ga.clearFiles(ip);
				}
			}
			if (guardarMPP(mp1)) {

			}
		}
		else {
			setVector(datas);
			this->mpp = mp1;
			this->wait = false;
			posG = pos;
			
		}
	}else if (head == "1503600" || head == "1503800") {
		//cargar el identificadorde alarma para  //alarma fisiologica y de sistema
		MindrayAlarma ma;
		if (head == "1503600") {
			ma.setTipo(1);
		}
		else {
			ma.setTipo(0);
		}
		ma.setFuente(ip);
		ma.setDtaTime(this->dat_time);
	
		HeaderTram he = ma.getHead();
		he.loadHead(datas, pos);
		if (datas.size() >= he.sizePacket()) {
		ma.setHead(he);
		pos=ma.clasifyData(datas, pos);
	
		if (guardarMA(ma)) {
			
			}
		}
		else {
			setVector(datas);
			this->ma = ma;
			this->wait = false;
			posG = pos;
		}
	}
	else {
		if (datWait.empty() == false) {
			datWait.insert(std::end(datWait), std::begin(datas), std::end(datas));
			if (this->mp.getFuente().empty() == false) {
				pos = mp.clasifyData(datWait, posG);
				if (alm.mp.getDataTime() == mp.getDataTime()) {
					this->alm.mp = mp;
					
					alm.mp.setSubTrama(mp.getSubTra());
				}
				guardarMP(mp);
				
				mp.getFuente().clear();
				datWait.clear();
			}

			else if (this->ma.getFuente().empty() == false) {
				ma.clasifyData(datWait, posG);
				if (ma.getDataTime() == alm.mpp.getDataTime()) {
					alm.ma = ma;
				}
				guardarMA(ma);
				
				ma.getFuente().clear();
				datWait.clear();
			}
			else if (this->mpp.getFuente().empty() == false) {
				mpp.clasifyData(datWait, posG);
				
				guardarMPP(mpp);
				
				mpp.getFuente().clear();
				datWait.clear();
			}
			else {

			}
		}
		else {

		}
	}
	
	return pos;
}


string  CapturaDeRed::captDta_time() {
	string data_ti;
	SYSTEMTIME  lt;
	GetLocalTime(&lt);
	ts.year = lt.wYear; ts.month = lt.wMonth; ts.day = lt.wDay; ts.hour = lt.wHour; ts.minute = lt.wMinute; ts.second = lt.wSecond; ts.fraction = lt.wMilliseconds;
	if (lt.wMonth < 10 || lt.wDayOfWeek < 10 || lt.wMinute < 10 ||lt.wSecond < 10) {
		if (lt.wMonth < 10) {
			data_ti = std::to_string(lt.wYear) + "-" + "0" + std::to_string(lt.wMonth) + "-";
		}
		else {
			data_ti = std::to_string(lt.wYear) + "-" + std::to_string(lt.wMonth) + "-";
		}
		if (lt.wDay < 10) {
			data_ti += "0" + std::to_string(lt.wDay);
		}
		else {
			data_ti += std::to_string(lt.wDay);
		}
		if (lt.wHour < 10) {
			data_ti += " 0" + std::to_string(lt.wHour);
		}
		else {
			data_ti += " " + std::to_string(lt.wHour);
			}
		if	(lt.wMinute < 10) {
			data_ti += ":0" + std::to_string(lt.wMinute);
		}
		else {
			data_ti += ":" + std::to_string(lt.wMinute);
		}
		if (lt.wSecond < 10) {
			data_ti += ":0" + std::to_string(lt.wSecond)+ ":" +std::to_string(lt.wMilliseconds);
		}
		else {
			data_ti += ":" + std::to_string(lt.wSecond)+":" + std::to_string(lt.wMilliseconds);
		}
	}
	else {
		data_ti += std::to_string(lt.wYear) + "-" + std::to_string(lt.wMonth) + "-" + std::to_string(lt.wDayOfWeek) + " " + std::to_string(lt.wHour) + ":" + std::to_string(lt.wMinute) + ":" + std::to_string(lt.wSecond) + ":" +
			std::to_string(lt.wMilliseconds);
	}
	return  data_ti;
}


std::string CapturaDeRed::capDta_timeforHl7()
{
	string data_ti;
	SYSTEMTIME  lt;
	GetLocalTime(&lt);
	ts.year = lt.wYear; ts.month = lt.wMonth; ts.day = lt.wDay; ts.hour = lt.wHour; ts.minute = lt.wMinute; ts.second = lt.wSecond; ts.fraction = lt.wMilliseconds;
	if (lt.wMonth < 10 || lt.wDayOfWeek < 10 || lt.wMinute < 10 || lt.wSecond < 10) {
		if (lt.wMonth < 10) {
			data_ti = std::to_string(lt.wYear) +"0" + std::to_string(lt.wMonth);
		}
		else {
			data_ti = std::to_string(lt.wYear) + std::to_string(lt.wMonth);
		}
		if (lt.wDay < 10) {
			data_ti += "0" + std::to_string(lt.wDay);
		}
		else {
			data_ti += std::to_string(lt.wDay);
		}
		if (lt.wHour < 10) {
			data_ti += "0" + std::to_string(lt.wHour);
		}
		else {
			data_ti += std::to_string(lt.wHour);
		}
		if (lt.wMinute < 10) {
			data_ti += "0" + std::to_string(lt.wMinute);
		}
		else {
			data_ti += std::to_string(lt.wMinute);
		}
		if (lt.wSecond < 10) {
			data_ti += "0" + std::to_string(lt.wSecond) + "." + std::to_string(lt.wMilliseconds);
		}
		else {
			data_ti += std::to_string(lt.wSecond) + "." + std::to_string(lt.wMilliseconds);
		}
	}
	else {
		data_ti += std::to_string(lt.wYear) + std::to_string(lt.wMonth)  + std::to_string(lt.wDayOfWeek) + std::to_string(lt.wHour)  + std::to_string(lt.wMinute)  + std::to_string(lt.wSecond) + "." +
			std::to_string(lt.wMilliseconds);
	}
	return  data_ti;
}



void CapturaDeRed::setVector(const vector<uint8_t> &v) {
	this->datWait = v;
}

 vector<uint8_t> CapturaDeRed::getDataWait() {
	return this->datWait;
}


int CapturaDeRed::searchHead(const vector<uint8_t> &datas, int pos) {
	string hed;
	stringstream posib;
	bool ban = false;
	int cont = 0;
	uint32_t var=0;
	uint32_t var2=0;
	for (pos; pos < datas.size()  && pos+6 <datas.size() && ban==false ;pos++) {
		cont = pos;
		var = 0;
		var2 = 0;
		var = var | datas.at(++cont);
		var = var << 16;
		var = var | datas.at(++cont);
		var = var << 8;
		var = var | datas.at(++cont);
		var2 = var2 | datas.at(++cont);
		var2 = var2 << 16;
		var2 = var2 | datas.at(++cont);
		var2 = var2 << 8;
		var2 = var2 | datas.at(++cont);
		//var = ((datas.at(++cont) << 16)  | (datas.at(++cont) << 8) | (datas.at(++cont)));
		//var2 = ((datas.at(++cont) << 16) | (datas.at(++cont) << 8) | (datas.at(++cont)));
		posib << std::hex <<var;
		posib << std::hex << var2;
		hed = posib.str();
		posib.str("");
		
		if (hed == "10005009d000000") {
			ban = true;
			
			pos = cont - 6;
		}
		else if (hed == "1000500cc000000"){
			LOGI("[CapturaDeRed] cabeza de entrada para entrar")
			ban = true;
			pos = cont - 6;
		}
		else if (hed == "100050036000000" || hed == "100050038000000") {
			
			ban = true;
			pos = cont - 6;
		}
		else {
			hed.clear();
		} 
	}
	posib.clear();
	hed.clear();
	return pos;
}


bool CapturaDeRed::guardarMP(MindrayPacket &mp) {
	Signal sig;
	
	if (this->ga.searchIp(mp.getFuente())) {
		for (int i = 0; i < mp.getSubTra().size(); i++)
		{
		ga.EscribirDatSig(mp.getSubTra().at(i)->datTram(sig),mp);
		delete mp.getSubTra().at(i);
		}
}
else {
	this->ga.crearArchivo(mp.getFuente());
	for (int i = 0; i < mp.getSubTra().size(); i++)
	{
		ga.EscribirDatSig(mp.getSubTra().at(i)->datTram(sig),mp);
		delete mp.getSubTra().at(i);
		}
	}
return true;
}




bool CapturaDeRed::guardarMPP(MindrayParametros &mp) {
	Impedancia imp;
	ECG ecg;
	Art art;
	Ap ap;
	SPO2 spo2;
	Temp temp;
	if (this->ga.searchIp(mp.getFuente())) {
		
		for (int i = 0; i < mp.getSubTra().size(); i++)
		{
			SubTramaECG * sue = dynamic_cast<SubTramaECG *> (mp.getSubTra().at(i));
			if (sue != NULL) {
				ga.EscribirDatECG(sue-> datTram(ecg), mp);
				delete mp.getSubTra().at(i);
			}
			else {
				SubTramaImpedancia * sue1 = dynamic_cast<SubTramaImpedancia *> (mp.getSubTra().at(i));
				if (sue1 != NULL) {
					ga.EscribirDatImpedancia(sue1->datTram(imp), mp);
					delete mp.getSubTra().at(i);
				}
				else {
					SubTramSpo2 * sue2 = dynamic_cast<SubTramSpo2 *> (mp.getSubTra().at(i));
					if (sue2 != NULL ) {
						ga.EscribirDatSpo2(sue2->datTram(spo2), mp);
						delete mp.getSubTra().at(i);
					}
					else {
						SubtRamTemp * sue3 = dynamic_cast<SubtRamTemp *> (mp.getSubTra().at(i));
						if (sue3 != NULL) {
							ga.EscribirDatTemp(sue3->datTram(temp), mp);
							delete mp.getSubTra().at(i);
						}
						else {
							SubTramaArt_AP * sue4 = dynamic_cast<SubTramaArt_AP *> (mp.getSubTra().at(i));
							if (sue4 !=NULL) {
								if (sue4->isBand()) {
									ga.EscribirDatAp(sue4->datTram(ap), mp);
									delete mp.getSubTra().at(i);
								}
								else {
									ga.EscribirDatArt(sue4->datTram1(art), mp);
									delete mp.getSubTra().at(i);
								}
							}
							else {
								delete mp.getSubTra().at(i);
							}
						}
					}
				}
			}
		}
	}
	else {
	this->ga.crearArchivo(mp.getFuente());
		for (int i = 0; i < mp.getSubTra().size(); i++)
		{
			SubTramaECG * sue = dynamic_cast<SubTramaECG *> (mp.getSubTra().at(i));
			if (sue != NULL) {
				ga.EscribirDatECG(sue->datTram(ecg), mp);
				delete mp.getSubTra().at(i);
			}
			else {
				SubTramaImpedancia * sue1 = dynamic_cast<SubTramaImpedancia *> (mp.getSubTra().at(i));
				if (sue1 != NULL) {
					ga.EscribirDatImpedancia(sue1->datTram(imp), mp);
					delete mp.getSubTra().at(i);
				}
				else {
					SubTramSpo2 * sue2 = dynamic_cast<SubTramSpo2 *> (mp.getSubTra().at(i));
					if (sue2 != NULL) {
						ga.EscribirDatSpo2(sue2->datTram(spo2), mp);
						delete mp.getSubTra().at(i);
					}
					else {
						SubtRamTemp * sue3 = dynamic_cast<SubtRamTemp *> (mp.getSubTra().at(i));
						if (sue != NULL) {
							
							ga.EscribirDatTemp(sue3->datTram(temp), mp);
							delete mp.getSubTra().at(i);
						}
						else {
							SubTramaArt_AP * sue4 = dynamic_cast<SubTramaArt_AP *> (mp.getSubTra().at(i));
							if (sue4 != NULL) {
								if (sue4->isBand()) {
									ga.EscribirDatAp(sue4->datTram(ap), mp);
									delete mp.getSubTra().at(i);
								}
								else {
									ga.EscribirDatArt(sue4->datTram1(art), mp);
									delete mp.getSubTra().at(i);
								}
							}
							else {
								delete mp.getSubTra().at(i);
							}
						}
					}
				}
			}
		}
	}
return true;
}



bool CapturaDeRed::guardarMA(MindrayAlarma &ma) {
	if (this->ga.searchIp(ma.getFuente())) {
		for (int i = 0; i < ma.getSubTra().size(); i++)
		{
			this->ga.EscribirDatALarma(ma.getSubTra().at(i)->getMensajes(), ma);
			delete ma.getSubTra().at(i);
		}
	}
	else {
		this->ga.crearArchivo(ma.getFuente());
		for (int i = 0; i < ma.getSubTra().size(); i++)
		{
			this->ga.EscribirDatALarma(ma.getSubTra().at(i)->getMensajes(), ma);
			delete ma.getSubTra().at(i);
		}
	}
	return true;
}

void CapturaDeRed::almacenDB() {
	try {
		if (!co.isOpen()) {
			co.OpenCo();
			LOGI("[CapturaDeRed] ABRI LA BASE DE DATOS, Inicia Proceso de almacenado");
		}
		if (!co.isConect2()) {
			co.loadOtherDat();
		}
		co.setTimeStruc(this->ts);
		co.loadDatTableMon();
		co.insertaDatTab();
	}
	catch (std::bad_alloc &al) 
	{
		LOGW("Se presento un erro en base de datos " << al.what());
	}
}
