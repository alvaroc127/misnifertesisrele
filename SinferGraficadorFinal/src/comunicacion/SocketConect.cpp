#include "..\Include\comunicacion\SocketConect.h"


SocketConect::SocketConect() 
{

}


SocketConect::~SocketConect()
{

}

void SocketConect::setInterface()
{
	BaseDB::ga.loadPropertis();
	//this->inter = et;
}

void SocketConect::sendandRecived( std::string &stream) 
{
	try
	{
		//LOGD("[SocketConnect] el connect es " << conect );
		if (false== conect)
		{
			sock.conectar(BaseDB::ga.getPropertyConfig("SOCKET.IP"),
				BaseDB::ga.getPropertyConfig("SOCKET.PORT.DEST"));
			
			//LOGD("[SocketConnect] el connect es " << conect);
			conect = true;
		}
		LOGA("Se enviara el mensaje <<" << stream);
		//int portsourc = atoi(BaseDB::ga.getPropertyConfig("SOCKET.PORT.SOURCE").c_str());
		//int portDest = atoi(BaseDB::ga.getPropertyConfig("SOCKET.PORT.DEST").c_str());
		 //HL7MLLP sock(BaseDB::ga.getPropertyConfig("SOCKET.IP"),
			//BaseDB::ga.getPropertyConfig("SOCKET.PORT.DEST").c_str());
		size_t out=sock.send_msg_mllp(stream);
		
		
		if (-1 == out) 
		{
			conect = false;
			sock.closeS();
			throw HL7Exception("[SocketConnect] Envio Parcial de datos");
		}
	}
	catch (HL7Exception &ex) 
	{
		conect = false;
		throw ex;
	}
	catch (...) {
		conect = false;
		LOGW("[SocketConnect] Se genero una excepcion en la conexion");
		throw std::exception();
	}
	

}



void SocketConect::sendandRecived(std::string &stream,std::string &port)
{
	try
	{
		//LOGD("[SocketConnect] el connect es " << conect );
		if (false == conect)
		{
			sock.conectar(BaseDB::ga.getPropertyConfig("SOCKET.IP"),
				BaseDB::ga.getPropertyConfig(port));

			//LOGD("[SocketConnect] el connect es " << conect);
			conect = true;
		}
		LOGA("Se enviara el mensaje <<" << stream);
		//int portsourc = atoi(BaseDB::ga.getPropertyConfig("SOCKET.PORT.SOURCE").c_str());
		//int portDest = atoi(BaseDB::ga.getPropertyConfig("SOCKET.PORT.DEST").c_str());
		 //HL7MLLP sock(BaseDB::ga.getPropertyConfig("SOCKET.IP"),
			//BaseDB::ga.getPropertyConfig("SOCKET.PORT.DEST").c_str());
		size_t out = sock.send_msg_mllp(stream);


		if (-1 == out)
		{
			conect = false;
			sock.closeS();
			throw HL7Exception("[SocketConnect] Envio Parcial de datos");
		}
	}
	catch (HL7Exception &ex)
	{
		conect = false;
		throw ex;
	}
	catch (...) {
		conect = false;
		LOGW("[SocketConnect] Se genero una excepcion en la conexion");
		throw std::exception();
	}


}