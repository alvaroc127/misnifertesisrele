/*
 * This file is part of Auriga HL7 library.
 *
 * Auriga HL7 library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Auriga HL7 library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Auriga HL7 library.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <../Include/comunicacion/hl7socket.h>
#include <log4z.h>



HL7Socket::HL7Socket() {}

HL7Socket::HL7Socket(const std::string& host,const std::string& port)
	{
		addrinfo hints;
        WSADATA	wsaData;
		char buf[300];
        int wsa_err = WSAStartup( MAKEWORD( 2, 2 ), &wsaData );
        if ( wsa_err )
        {
             //std::cerr << "Error: WSAStartup:" << gai_strerror( wsa_err ) << std::endl;
			LOGW("[SocketHl7] Fallo iniciando el socket " << gai_strerror(wsa_err));
			throw HL7Exception(gai_strerror(wsa_err));
             //exit( EXIT_FAILURE );
        }

		memset(&hints, 0, sizeof(hints));
		hints.ai_family = AF_UNSPEC;    /* Allow IPv4 or IPv6 */
		hints.ai_socktype = SOCK_STREAM; /* Datagram socket */
		hints.ai_flags = 0;
		hints.ai_protocol = 0;          /* Any protocol */

		addrinfo *result;
		int s = getaddrinfo(
				host.c_str(), 
				port.c_str(), 
				&hints, 
				&result);

		if (s != 0)
		{
			LOGW("[SocketHl7] se genero un error getaddrinfo " << gai_strerror(s));
			//std::cerr<<"Error: getaddrinfo:"<<gai_strerror(s)<<std::endl;
            WSACleanup();
			throw HL7Exception(gai_strerror(s));
            //exit( EXIT_FAILURE );
		}
		
		addrinfo * rp;
		for (rp = result; rp != NULL; rp = rp->ai_next)
		{
			 _socketfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
			if (_socketfd == -1)
				continue;
			if (connect(_socketfd, rp->ai_addr, rp->ai_addrlen) != -1)
				break;
			closeso(_socketfd);
		}
		if (rp == NULL)
		{
			//std::cerr<<"Error: Could not connect"<<std::endl;
			LOGW("[SocketHl7] imposible conectarse con el socket  " << gai_strerror(s));
            WSACleanup();
			throw HL7Exception(gai_strerror(s));
            //exit( EXIT_FAILURE );
		}
		freeaddrinfo(result);
	}

	HL7Socket::HL7Socket( int new_sock ){ _socketfd = new_sock; }

	void HL7Socket::conectar(const std::string& host, const std::string& port)
	{
		addrinfo hints;
		WSADATA	wsaData;
		char buf[300];
		int wsa_err = WSAStartup(MAKEWORD(2, 2), &wsaData);
		if (wsa_err)
		{
			//std::cerr << "Error: WSAStartup:" << gai_strerror( wsa_err ) << std::endl;
			LOGW("[SocketHl7] Fallo iniciando el socket " << gai_strerror(wsa_err));
			throw HL7Exception(gai_strerror(wsa_err));
			//exit( EXIT_FAILURE );
		}

		memset(&hints, 0, sizeof(hints));
		hints.ai_family = AF_UNSPEC;    /* Allow IPv4 or IPv6 */
		hints.ai_socktype = SOCK_STREAM; /* Datagram socket */
		hints.ai_flags = 0;
		hints.ai_protocol = 0;          /* Any protocol */

		addrinfo *result;
		int s = getaddrinfo(
			host.c_str(),
			port.c_str(),
			&hints,
			&result);

		if (s != 0)
		{
			LOGW("[SocketHl7] se genero un error getaddrinfo " << gai_strerror(s));
			//std::cerr<<"Error: getaddrinfo:"<<gai_strerror(s)<<std::endl;
			WSACleanup();
			throw HL7Exception(gai_strerror(s));
			//exit( EXIT_FAILURE );
		}

		addrinfo * rp;
		for (rp = result; rp != NULL; rp = rp->ai_next)
		{
			_socketfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
			if (_socketfd == -1)
				continue;
			if (connect(_socketfd, rp->ai_addr, rp->ai_addrlen) != -1)
				break;
			closeso(_socketfd);
		}
		if (rp == NULL)
		{
			//std::cerr<<"Error: Could not connect"<<std::endl;
			LOGW("[SocketHl7] imposible conectarse con el socket  " << gai_strerror(s));
			WSACleanup();
			throw HL7Exception(gai_strerror(s));
			//exit( EXIT_FAILURE );
		}
		freeaddrinfo(result);
	}


	HL7Socket::~HL7Socket()
	{
		closeso(_socketfd);
		WSACleanup();
    }

	size_t HL7Socket::send_msg(const std::string& data)
		{
			size_t len = write(_socketfd, data.c_str(), data.length());
			//std::cout<<len<<std::endl;
			if (len != data.length())
			{
				//std::cerr<<"Error: partial/failed write"<<std::endl;
				LOGW(" Escritura parical del mensaje ");
			}
			return len;
		}

    size_t HL7Socket::read_msg(std::string& data)
		{
            char buffer[ MAX_INSIDE_BUFFER_LEN + 1 ];
            const size_t max_len = MAX_INSIDE_BUFFER_LEN;

			bzero(buffer, max_len + 1);

            size_t n = read_soc( _socketfd, buffer, max_len );
            if ( n == -1 )
                 return n;
			//std::cout<<"READ:"<<n<<std::endl;
			data = buffer;
            while ( n > 0 )
            {
                 bzero( buffer, max_len + 1 );
                 if ( 0 != ioctl( _socketfd, FIONREAD, (int*)&n ) )
                 {
                      return -1;
                 }
                 if ( n != 0 )
                 {
                      n = read_soc( _socketfd, buffer, max_len );
                      if ( n == -1 )
                           return n;
                      data += buffer;
                 }
            }

		  return data.length();
	 }

int HL7Socket::getSocket(){ return _socketfd; }


size_t HL7Socket::write(int fd, const void *buf, size_t count)
{
	return (size_t)send((SOCKET)fd, (const char*)buf, (int)count, 0);
}

size_t HL7Socket::read_soc(int fd, const void *buf, size_t count)
{
	return (size_t)recv((SOCKET)fd, (char*)buf, (int)count, 0);
}

 int HL7Socket::ioctl(int fd, unsigned long cmd, int* argp)
{
	return ioctlsocket((SOCKET)fd, cmd, (unsigned long*)argp);
}

void HL7Socket::bzero(char* buffer, int max_len)
{
	memset(buffer, 0, max_len);
	return;
}


//#ifdef TESTS
//    void setSocket( int new_sock ){ _socketfd = new_sock; }
//#endif // TESTS




/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 * =====================================================================================
 */
/*
void socket_test()
{
	std::string hl7_message = "MSH|^~\\&|system1|W|system2|UHN|200105231927||ADT^A01^ADT_A01|22139243|P|2.4\t\
EVN|A01|200105231927|\t\
PID||9999999999^^|2216506^||Duck^Donald^^^MR.^MR.||19720227|M|||123 Foo ST.^^TORONTO^ON^M6G 3E6^CA^H^~123 Foo ST.^^TORONTO^ON^M6G 3E6^CA^M^|1811|( 416 )111 - 1111||E^ENGLISH|S|PATIENT DID NOT INDICATE|211004554^||||||||||||\t\
PV1|||ZFAST TRACK^WAITING^13|E^EMERGENCY||369^6^13^U EM EMERGENCY DEPARTMENT^ZFAST TRACK WAITING^FT WAIT 13^FTWAIT13^FT WAITING^FTWAIT13|^MOUSE^MICKEY^M^^DR.^MD|||SUR||||||||I|211004554^||||||||||||||||||||W|||||200105231927|||||\t\
PV2||F|^R / O APPENDICIAL ABSCESS|||||||||||||||||||||||||\t\
IN1|1||001001|OHIP||||||||||||^^^^^|||^^^^^^M^|||||||||||||||||||||||||^^^^^^M^|||||\t\
ACC|";

	std::string host = "192.168.150.8";
	std::string port = "6661";

	HL7Socket hs(host, port);
	hs.send_msg(hl7_message);

	//HL7SocketServer server(1234);
	//server.run()
}*/