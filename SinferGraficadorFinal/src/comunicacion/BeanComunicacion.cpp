#include "../Include/comunicacion/BeanComunicacion.h"



BeanComunicacion::BeanComunicacion() 
{
	
}


BeanComunicacion::~BeanComunicacion() {

}

void BeanComunicacion::createMsgHl7(const std::string &ip,const std::string &date_time)
{
	try {
		mapper.setDate(date_time);
		mapper.addToMessage(ip);
		appendVector(mapper.getpipeOUT());
		procalarm();
		sendMensaje();
		mapper.clean();
		//LOGD("se cargo ech " << vectOut[0]);
		
		//LOGD("se cargo frecresp " << vectOut[1]);
		
		//LOGD("se cargo ROJAN " << vectOut[2]);
		
		//LOGD("se cargo spo2 " << vectOut[3]);
		
		//LOGD("se cargo obxtemp  " << vectOut[4]);
		
		
	}
	catch (HL7Exception ex) 
	{
		LOGW("[BeanComunicacion ]ERROR en e el mappeador--01"<<ex.m_what);
	}
	catch (...) {
		LOGW("[BeanComunicacion ]ERROR DESCONOCIDO --02, ELIMINE EL CATCH.PARA SABER MAS");
	}
}


void BeanComunicacion::procalarm()
{
	std::vector<std::string> outarlm;
	outarlm.push_back(vec[0]);
	try 
	{
		std::vector<std::string> vectAlrm = mapper.getAlarm();
		if (!vectAlrm.empty() && vectAlrm.size() > 1)
		{
			outarlm.insert(outarlm.end(), vectAlrm.begin(), vectAlrm.end());
			while (!outarlm.empty())
			{
				alarsock.sendandRecived(outarlm.front(),std::string("SOCKET.PORT.DEST2"));
				outarlm.erase(outarlm.begin());
			}

		}// enviamos y adicionamos alarma en el inicio
	}
	catch (HL7Exception &ex)
	{
		LOGW("[Comunicaction error]fallo la conexion con el socket" << ex.m_what);
		throw ex;
	}

}

GestorArchivo * BeanComunicacion::getGa() {
	return ga;
}

void  BeanComunicacion::setgGa(GestorArchivo *lo) {
	ga = lo;
}


void BeanComunicacion::loadFile() {
	mapper.loadFile();
}


void BeanComunicacion::setInterface()
{	
	
	sock.setInterface();
	alarsock.setInterface();
}

void BeanComunicacion::appendVector(const std::map<std::string, std::vector<std::string>> &map)
{
	vec = map.find(ga->getPropertyConfig("HEAD.HL7"))->second;
	std::vector<std::string> vec2 = map.find(ga->getPropertyConfig("BODY.HL7"))->second;
	//LOGD("[Comunicaction] valor del vec2 antes del insert " << vec.size());
	vec.insert(vec.end(), vec2.begin(), vec2.end());
	//LOGD("[Comunicaction] valor del vec2 despues del insert " << vec.size());
	
}


void BeanComunicacion::sendMensaje()
{
	try {
		std::string send;
		//LOGD("[Comunicaction] se procesaran " << vec.size());
		while (!vec.empty())
		{
			send=vec.front();
			//LOGD("[Comunicaction] valor del vec " << send);
			sock.sendandRecived(send);
			vec.erase(vec.begin());
		}
		//LOGD("[Comunicaction] se enviara " << send);
		send.clear();
	}
	catch (HL7Exception &ex) 
	{
		LOGW("[Comunicaction error]fallo la conexion con el socket" << ex.m_what);
		throw ex;
	}
}