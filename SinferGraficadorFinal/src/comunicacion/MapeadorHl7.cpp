#include "../Include/comunicacion/MapeadorHl7.h"


MapeadorHl7::MapeadorHl7() {
	
}

MapeadorHl7::~MapeadorHl7() {


}

void MapeadorHl7::addToMessage(const std::string &ip)
{
	//generar una alerta con todos los mensajes
	try {
		std::vector<std::string> vecthead;
		if (!strout.empty())strout.clear();
		loadfileData(ip);
		strout.append(loadMSH());
		strout.append(loadPID());
		strout.append(loadPV1());
		strout.append(loadOBR());
		vecthead.push_back(strout);
		//LOGD("[Mappeador] head del mensaje" << strout);
		map[BaseDB::ga.getPropertyConfig("HEAD.HL7")] = vecthead;
		map[BaseDB::ga.getPropertyConfig("BODY.HL7")] = loadOBX();
	}
	catch (HL7Exception hl7x) {
		LOGE("NO se puedo lanzar crear el mappear el mensaje" << hl7x.what());
		clean();
		throw hl7x;
	}
	catch (...) 
	{
		clean();
		LOGW("Se lanzo una excepcion no contralada");
	}
}

void MapeadorHl7::setgGa(GestorArchivo * gar) {
	ga = gar;
}


void MapeadorHl7::loadfileData(const std::string &ip)
{
	mon.setId(-99);
	mon.setIp(ip);
	ecg.loadECG(&mon);
	spo2.loadSPO2(&mon);
	frec.loadFrecRes(&mon);
	temp.loadTemp(&mon);
	alrm.loadAlarm(&mon, &ecg);
}

std::string MapeadorHl7::loadMSH() 
{
	try {
		std::string convert;
		HL7_24::ORU_R01 MSG;
		HL7_24::MSH * msh = MSG.getMSH_1();
		//MSG.getPATIENT_RESULT()->getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		msh->getMSH_1()->setData(std::string("|").c_str());
		msh->getMSH_2()->setData(std::string("^~\\&").c_str());
		msh->getMSH_3()->getHD_1()->setData(std::string(""));
		msh->getMSH_3()->getHD_2()->setData(BaseDB::ga.getPropertyConfig("MSH.TRANS.SOURCE").c_str());
		msh->getMSH_3()->getHD_3()->setData(std::string(""));
		msh->getMSH_4()->getHD_1()->setData(std::string(""));
		msh->getMSH_4()->getHD_2()->setData(std::string(""));
		msh->getMSH_4()->getHD_3()->setData(std::string(""));
		msh->getMSH_5()->getHD_1()->setData(std::string(""));
		msh->getMSH_5()->getHD_2()->setData(std::string(""));
		msh->getMSH_5()->getHD_3()->setData(std::string(""));
		msh->getMSH_6()->getHD_1()->setData(std::string(""));
		msh->getMSH_6()->getHD_2()->setData(std::string(""));
		msh->getMSH_6()->getHD_3()->setData(std::string(""));

		msh->getMSH_7()->getTS_1()->setData(std::string(""));
		msh->getMSH_8()->setData(std::string(""));
		msh->getMSH_9()->getMSG_1()->setData("ORU");
		msh->getMSH_9()->getMSG_2()->setData("R01");
		msh->getMSH_10()->setData(UUID4().c_str());
		msh->getMSH_11()->getProcessingID()->setData("P");
		msh->getMSH_12()->getVersionID()->setData(std::string("2.3.1").c_str());
		segToPipe(msh,convert);
		convert.insert(0, "<VT>");
		convert.append("<CR>");
		MSG.clear();
		return convert;
	}
	catch (HL7Exception &ex) 
	{
		LOGW("[MSH] se lanzo una excepcion en el la carga de dataa MSH " << ex.what()<< ex.m_what);
		throw ex;
	}

}

std::string MapeadorHl7::loadPID()
{
	try
	{
		HL7_24::ORU_R01 MSG;
		std::string load;
		HL7_24::PID * Pid = MSG.getPATIENT_RESULT()->
			getPATIENT()->
			getPID_4();
		Pid->getPID_1()->setData("");
		Pid->getPID_2()->getCX_1()->setData("");
		Pid->getPID_3()->getCX_1()->setData("PATIENT");
		Pid->getPID_3()->getCX_2()->setData(mon.getIp());
		Pid->getPID_4()->getCX_1()->setData("");
		Pid->getPID_5()->getFamilyName()->getFN_1()->setData
			(BaseDB::ga.getPropertyConfig("patient.no.name").c_str());
		Pid->getPID_6()->getXPN_1()->getFN_1()->setData(std::string(""));
		Pid->getPID_7()->getTS_1()->setData(date_time.c_str());
		Pid->getPID_8()->setData(BaseDB::ga.getPropertyConfig("patient.no.sex").c_str());
		Pid->getPID_9()->getXPN_1()->getFN_1()->setData(std::string(""));
		Pid->getPID_10()->getAlternateText()->setData(std::string(""));
		Pid->getPID_11()->getXAD_1()->getSAD_1()->setData(std::string(""));
		Pid->getPID_11()->getXAD_2()->setData(std::string(""));
		Pid->getPID_11()->getXAD_3()->setData(std::string(""));
		Pid->getPID_11()->getXAD_4()->setData(std::string(""));
		Pid->getPID_11()->getXAD_5()->setData(std::string(""));
		Pid->getPID_11()->getXAD_6()->setData(std::string(""));
		Pid->getPID_11()->getXAD_7()->setData(std::string(""));
		Pid->getPID_11()->getXAD_8()->setData(BaseDB::ga.getPropertyConfig("patient.no.addres").c_str());
		Pid->getPID_12()->setData(std::string(""));
		Pid->getPID_13()->getXTN_1()->setData(std::string(""));
		Pid->getPID_13()->getXTN_2()->setData(std::string(""));
		Pid->getPID_13()->getXTN_3()->setData(std::string(""));
		Pid->getPID_13()->getXTN_4()->setData(std::string(""));
		Pid->getPID_13()->getXTN_5()->setData(std::string(""));
		Pid->getPID_13()->getXTN_6()->setData(std::string(""));
		Pid->getPID_13()->getXTN_7()->setData(std::string(""));
		Pid->getPID_13()->getXTN_8()->setData(std::string(""));
		Pid->getPID_13()->getXTN_9()->setData(BaseDB::ga.getPropertyConfig("patient.no.tel").c_str());
		segToPipe(Pid, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		MSG.clear();
		return load;
	}
	catch (HL7Exception  &exhl7) {
		LOGW("[PID] se genero una excepcion en PID"<<exhl7.what());
		throw exhl7;
	}

}



std::string MapeadorHl7::loadPV1()
{
	try
	{
		std::string cad;
		cad = cad.append("UCI");
		cad = cad.append("&");
		cad = cad.append(mon.getIp());
		cad = cad.append("&");
		cad = cad.append("0");
		cad = cad.append("&");
		cad = cad.append("0");
		HL7_24::ORU_R01 MSG;
		HL7_24::PV1 * pv1 = MSG.getPATIENT_RESULT()->
			getPATIENT()->
			getVISIT()->
			getPV1_19();
		pv1->getPV1_1()->setData(std::string(""));
		pv1->getPV1_2()->setData(BaseDB::ga.getPropertyConfig("PV1.OUT"));
		pv1->getPV1_3()->getBed()->setData(cad);
		pv1->getPV1_4()->setData(std::string(""));
		pv1->getPV1_5()->getCX_1()->setData(std::string(""));
		pv1->getPV1_6()->getPL_1()->setData(std::string(""));
		pv1->getPV1_7()->getXCN_1()->setData(std::string(""));
		pv1->getPV1_7()->getXCN_2()->getFN_1()->setData(std::string(""));
		pv1->getPV1_7()->getXCN_3()->setData(BaseDB::ga.getPropertyConfig("PV1.no.attending.doctor"));
		pv1->getPV1_8()->getXCN_1()->setData(std::string(""));
		pv1->getPV1_9()->getXCN_1()->setData(std::string(""));
		pv1->getPV1_10()->setData(std::string(""));
		pv1->getPV1_11()->getPL_1()->setData("");
		pv1->getPV1_12()->setData(std::string(""));
		pv1->getPV1_13()->setData(std::string(""));
		pv1->getPV1_14()->setData(std::string(""));
		pv1->getPV1_15()->setData(std::string(""));
		pv1->getPV1_16()->setData(std::string(""));
		pv1->getPV1_17()->getXCN_1()->setData(std::string(""));
		pv1->getPV1_18()->setData(BaseDB::ga.getPropertyConfig("PV1.TYPE"));
		pv1->getPV1_19()->getCX_1()->setData(std::string(""));
		pv1->getPV1_20()->getFC_1()->setData(std::string(""));
		pv1->getPV1_21()->setData(std::string(""));
		pv1->getPV1_22()->setData(std::string(""));
		pv1->getPV1_23()->setData(std::string(""));
		pv1->getPV1_24()->setData(std::string(""));
		pv1->getPV1_25()->setData(std::string(""));
		pv1->getPV1_26()->setData(std::string(""));
		pv1->getPV1_27()->setData(std::string(""));
		pv1->getPV1_28()->setData(std::string(""));
		pv1->getPV1_29()->setData(std::string(""));
		pv1->getPV1_30()->setData(std::string(""));
		pv1->getPV1_31()->setData(std::string(""));
		pv1->getPV1_32()->setData(std::string(""));
		pv1->getPV1_33()->setData(std::string(""));
		pv1->getPV1_34()->setData(std::string(""));
		pv1->getPV1_35()->setData(std::string(""));
		pv1->getPV1_36()->setData(std::string(""));
		pv1->getPV1_37()->getDLD_1()->setData(std::string(""));
		pv1->getPV1_38()->getAlternateText()->setData(std::string(""));
		pv1->getPV1_39()->setData(std::string(""));
		pv1->getPV1_40()->setData(std::string(""));
		pv1->getPV1_41()->setData(std::string(""));
		pv1->getPV1_42()->getPL_1()->setData(std::string(""));
		pv1->getPV1_43()->getPL_1()->setData(std::string(""));
		pv1->getPV1_44()->getTS_1()->setData(date_time);
		cad.swap(std::string(""));
		segToPipe(pv1,cad);
		cad.insert(0, "<VT>");
		cad.append("<CR>");
		MSG.clear();
		return cad;
	}
	catch (HL7Exception  &exhl7) {
		LOGW("[PV1] se genero una excepcion en PV1" << exhl7.what());
		throw exhl7;
	}

}


std::string MapeadorHl7::loadOBR()
{
	try
	{
		std::string load;
		HL7_24::ORU_R01 MSG;
		HL7_24::OBR * obr = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->
			getOBR_29();
		obr->getOBR_1()->setData(std::string(""));
		obr->getOBR_2()->getEI_1()->setData(std::string(""));
		obr->getOBR_3()->getEI_1()->setData(std::string(""));
		obr->getOBR_4()->getCE_1()->setData(std::string(""));
		obr->getOBR_4()->getCE_2()->setData(std::string(""));
		obr->getOBR_4()->getCE_3()->setData(std::string(""));
		obr->getOBR_4()->getCE_4()->setData(std::string(""));
		obr->getOBR_4()->getAlternateText()->setData(BaseDB::ga.getPropertyConfig("OBR.UNIVERSALSERVICE"));
		obr->getOBR_5()->setData(std::string(""));
		obr->getOBR_6()->getTS_1()->setData(std::string(""));
		obr->getOBR_7()->getTS_1()->setData(date_time);
		segToPipe(obr, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		MSG.clear();
		return load;
	}
	catch (HL7Exception  &exhl7) 
	{
		LOGW("[OBR] se genero una excepcion en OBR" << exhl7.what());
		throw exhl7;
	}

}

std::vector<std::string> MapeadorHl7::loadOBX()
{
	std::string end = "@end@";
	std::string tailelem;
	try
	{
		std::vector<std::string> vectOut;
		std::vector<std::string> vectAlrm;
		vectOut.push_back(loadOBXECG());
		//LOGD("se cargo ech " << vectOut[0]);
		vectOut.push_back(loadOBXFrecResp());
		//LOGD("se cargo frecresp " << vectOut[1]);
		vectOut.push_back(loadOBXSiglRojaAmr());
		//LOGD("se cargo ROJAN " << vectOut[2]);
		vectOut.push_back(loadOBXSPO2());
		//LOGD("se cargo spo2 " << vectOut[3]);
		vectOut.push_back(loadOBXTemp());
		//LOGD("se cargo obxtemp  " << vectOut[4]);
		vectAlrm = loadOBXAlarm();
		if (!vectAlrm.empty() && vectAlrm.size() > 1) vectOut.insert(vectOut.end(), vectAlrm.begin(), vectAlrm.end());

		std::vector<std::string>::iterator iter = vectOut.end();
		--iter;
		tailelem = (*iter);
		tailelem = tailelem.append(end);
		vectOut.erase(iter);
		vectOut.push_back(tailelem);
		return vectOut;
	}
	catch (HL7Exception  &exhl7) 
	{
		LOGW("[OBX root] se genero una excepcion en OBX" << exhl7.what());
		throw exhl7;
	}
}

unsigned int MapeadorHl7::random_char()
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0, 255);
	return dis(gen);
}


HL7_24::OBX * MapeadorHl7::getOBXHl(HL7_24::OBX *obx1, const std::string  &observationiTex,const std::string  &observationid)
{
	try {
		obx1->getOBX_1()->setData(std::string(""));
		obx1->getOBX_2()->setData(BaseDB::ga.getPropertyConfig("OBX.VALUTYPE"));
		obx1->getOBX_3()->getCE_1()->setData(observationid);
		obx1->getOBX_3()->getCE_2()->setData(observationiTex);
		obx1->getOBX_6()->getCE_1()->setData("");
		obx1->getOBX_7()->setData(std::string(""));
		obx1->getOBX_8()->setData(std::string(""));
		obx1->getOBX_9()->setData(std::string(""));
		obx1->getOBX_10()->setData(std::string(""));
		obx1->getOBX_11()->setData("F");
		return obx1;
	}
	catch (HL7Exception &ex)
	{
		LOGW("[OBX_GEN] Se lanzo una excepcion en HL7 Mapeador "<< ex.what());
		throw ex;
	}
}

std::string MapeadorHl7::loadOBXECG()
{
	std::string load;
	std::string out;
	std::stringstream ss;
	std::vector<uint8_t>::iterator iteecg1;
	std::vector<uint8_t>::iterator iteecg2;
	std::vector<uint8_t>::iterator iteecg3;
	std::vector<uint8_t>ecgtem1 = ecg.getEcg1();
	std::vector<uint8_t>ecgtem2 = ecg.getEcg2();
	std::vector<uint8_t>ecgtem3 = ecg.getEcg3();
	try {
		HL7_24::ORU_R01 MSG;
		HL7_24::OBX* obxavf = MSG.getPATIENT_RESULT()->
		getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		
		obxavf=getOBXHl(obxavf, BaseDB::ga.getPropertyConfig("LLAVE.ECG"), BaseDB::ga.getPropertyConfig("CODIG.ECG"));
		obxavf->getObservationSubId()->setData(std::string("AVF").c_str());
		obxavf->getOBX_5()->setData(std::to_string(ecg.getaVF()));
		segToPipe(obxavf, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();
		

		HL7_24::OBX * obxavl = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		getOBXHl(obxavl, BaseDB::ga.getPropertyConfig("LLAVE.ECG"), BaseDB::ga.getPropertyConfig("CODIG.ECG"));
		obxavl->getObservationSubId()->setData("AVL");
		obxavl->getOBX_5()->setData(std::to_string(ecg.getAvl()));
		segToPipe(obxavl, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();

		HL7_24::OBX * obxAvr = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxAvr=getOBXHl(obxAvr, BaseDB::ga.getPropertyConfig("LLAVE.ECG"), BaseDB::ga.getPropertyConfig("CODIG.ECG"));
		obxAvr->getObservationSubId()->setData("AVR");
		obxAvr->getOBX_5()->setData(std::to_string(ecg.getAvr()));
		segToPipe(obxAvr, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();

		HL7_24::OBX * obxCVP = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxCVP =getOBXHl(obxCVP, BaseDB::ga.getPropertyConfig("LLAVE.ECG"), BaseDB::ga.getPropertyConfig("CODIG.ECG"));
		obxCVP->getObservationSubId()->setData("CVP");
		obxCVP->getOBX_5()->setData(std::to_string(ecg.getCVP()));
		segToPipe(obxCVP, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();

		HL7_24::OBX * obxFrec_Cardi = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxFrec_Cardi=getOBXHl(obxFrec_Cardi, BaseDB::ga.getPropertyConfig("LLAVE.ECG"), BaseDB::ga.getPropertyConfig("CODIG.ECG"));
		obxFrec_Cardi->getObservationSubId()->setData("ECGFR");
		obxFrec_Cardi->getOBX_5()->setData(std::to_string(ecg.getFrec_Cardi()));
		segToPipe(obxFrec_Cardi,load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();

		HL7_24::OBX * obxI= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxI=getOBXHl(obxI, BaseDB::ga.getPropertyConfig("LLAVE.ECG"), BaseDB::ga.getPropertyConfig("CODIG.ECG"));
		obxI->getObservationSubId()->setData("ECGI");
		obxI->getOBX_5()->setData(std::to_string(ecg.getI()));
		segToPipe(obxI, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();

		HL7_24::OBX * obxII = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxII=getOBXHl(obxII, BaseDB::ga.getPropertyConfig("LLAVE.ECG"), BaseDB::ga.getPropertyConfig("CODIG.ECG"));
		obxII->getObservationSubId()->setData("ECGII");
		obxII->getOBX_5()->setData(std::to_string(ecg.getII()));
		segToPipe(obxII, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();


		HL7_24::OBX * obxIII= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxIII=getOBXHl(obxIII, BaseDB::ga.getPropertyConfig("LLAVE.ECG"), BaseDB::ga.getPropertyConfig("CODIG.ECG"));
		obxIII->getObservationSubId()->setData("ECGIII");
		obxIII->getOBX_5()->setData(std::to_string(ecg.getIII()));
		segToPipe(obxIII, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();


		HL7_24::OBX * obxV= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxV=getOBXHl(obxV, BaseDB::ga.getPropertyConfig("LLAVE.ECG"), BaseDB::ga.getPropertyConfig("CODIG.ECG"));
		obxV->getObservationSubId()->setData("ECGV");
		obxV->getOBX_5()->setData(std::to_string(ecg.getV()));
		segToPipe(obxV, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();

		//a1 << std::hex << v2;


		HL7_24::OBX * obxECG1= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxECG1=getOBXHl(obxECG1, BaseDB::ga.getPropertyConfig("LLAVE.ECG"), BaseDB::ga.getPropertyConfig("CODIG.ECG"));
		obxECG1->getObservationSubId()->setData("ECGECG1");
		for (iteecg1 = ecgtem1.begin(); iteecg1 < ecgtem1.end();iteecg1++)
		{
			ss << std::hex << *iteecg1;
			
		}
		obxECG1->getOBX_5()->setData(ss.str());
		obxECG1->getOBX_5()->getData();
		segToPipe(obxECG1,load);
		load.insert(0, "<VT>");
		load.append("<CR>");

		
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();
		

		ss.str(std::string());
		ss.clear();
		HL7_24::OBX  * obxECG2= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxECG2=getOBXHl(obxECG2, BaseDB::ga.getPropertyConfig("LLAVE.ECG"), BaseDB::ga.getPropertyConfig("CODIG.ECG"));
		obxECG2->getObservationSubId()->setData("ECGECG2");
		for (iteecg2 = ecgtem2.begin(); iteecg2 < ecgtem2.end(); iteecg2++)
		{
			ss << std::hex << *iteecg2;
		} 
		obxECG2->getOBX_5()->setData(ss.str());
		segToPipe(obxECG2, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();

		ss.str(std::string());
		ss.clear();
		HL7_24::OBX *obxECG3= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxECG3=getOBXHl(obxECG3, BaseDB::ga.getPropertyConfig("LLAVE.ECG"), BaseDB::ga.getPropertyConfig("CODIG.ECG"));
		obxECG3->getObservationSubId()->setData("ECGECG3");
		for (iteecg3 = ecgtem3.begin();iteecg3 < ecgtem3.end();iteecg3++) 
		{
			ss << std::hex << *iteecg3;
		} 
		obxECG3->getOBX_5()->setData(ss.str());
		segToPipe(obxECG3, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();
		ecgtem1.clear();
		ecgtem2.clear();
		ecgtem1.clear();
		MSG.clear();
		return out;
	}
	catch (HL7Exception &ex) {
		LOGW("[ECG] UNa excepcion se lanzao en  el mapeo de ECG "<<ex.what());
		throw ex;
	}
}





std::string MapeadorHl7::loadOBXSPO2()
{
	std::string load;
	std::string out;
	std::stringstream ss;
	std::vector<uint8_t>::iterator itersig;
	std::vector<uint8_t> vector = spo2.getSigna();
	HL7_24::ORU_R01 MSG;
	try {
		
		HL7_24::OBX * obxSp2FR=MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxSp2FR=getOBXHl(obxSp2FR, BaseDB::ga.getPropertyConfig("LLAVE.SPO2"), BaseDB::ga.getPropertyConfig("CODIG.SPO2"));
		obxSp2FR->getObservationSubId()->setData("SPO2FR");
		obxSp2FR->getOBX_5()->setData(std::to_string(spo2.getFrec_encia()));
		segToPipe(obxSp2FR,load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();

		HL7_24::OBX * obxSp2Des= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxSp2Des=getOBXHl(obxSp2Des, BaseDB::ga.getPropertyConfig("LLAVE.SPO2"), BaseDB::ga.getPropertyConfig("CODIG.SPO2"));
		obxSp2Des->getObservationSubId()->setData("SPO2DESC");
		obxSp2Des->getOBX_5()->setData(std::to_string(spo2.getDesconocido()));
		segToPipe(obxSp2Des,load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();
		

		ss.str(std::string());
		ss.clear();
		HL7_24::OBX * obxSig= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxSig=getOBXHl(obxSig, BaseDB::ga.getPropertyConfig("LLAVE.SPO2"), BaseDB::ga.getPropertyConfig("CODIG.SPO2"));
		obxSig->getObservationSubId()->setData("SPO2SIGN");
		char buf[3];
		buf[0] = 0, buf[1] = 0, buf[2] = 0;
		std::string s;
		while (!vector.empty())
		{
			sprintf_s(buf, "%02x", vector.front());
			s.append(buf);
			//printf("%X", vectoap.front());
			buf[0] = 0, buf[1] = 0, buf[2] = 0;
			vector.erase(vector.begin());
		}
		obxSig->getOBX_5()->setData(s);
		segToPipe(obxSig, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();
		vector.clear();
		MSG.clear();
		return out;
	}
	catch (HL7Exception &ex) { 
		LOGW("[SPO2] Se genero el una excepcion en SPO2" << ex.what());
		throw ex;
	}
}


std::string MapeadorHl7::loadOBXSiglRojaAmr()
{
	std::string out;
	std::string load;
	HL7_24::ORU_R01 MSG;
	try 
	{
		sen_roj.loadSArt(&mon);
		std::vector<uint8_t>::iterator sigart;
		std::vector<uint8_t> vectoart = sen_roj.getSignal();
		std::stringstream ss;
		HL7_24::OBX * obxArtMax=MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxArtMax=getOBXHl(obxArtMax, BaseDB::ga.getPropertyConfig("LLAVE.ROJA_AMARILLA"), BaseDB::ga.getPropertyConfig("CODIG.ROJA_AMARILLA"));
		obxArtMax->getObservationSubId()->setData("RJAMARTMAX");
		obxArtMax->getOBX_5()->setData(std::to_string(sen_roj.getMax()));
		segToPipe(obxArtMax,load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();

		HL7_24::OBX * obxArtMin= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxArtMin=getOBXHl(obxArtMax, BaseDB::ga.getPropertyConfig("LLAVE.ROJA_AMARILLA"), BaseDB::ga.getPropertyConfig("CODIG.ROJA_AMARILLA"));
		obxArtMin->getObservationSubId()->setData("RJAMARTMIN");
		obxArtMin->getOBX_5()->setData(std::to_string(sen_roj.getMin()));
		segToPipe(obxArtMin, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();


		HL7_24::OBX * obxArtPar= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxArtPar=getOBXHl(obxArtPar, BaseDB::ga.getPropertyConfig("LLAVE.ROJA_AMARILLA"), BaseDB::ga.getPropertyConfig("CODIG.ROJA_AMARILLA"));
		obxArtPar->getObservationSubId()->setData("RJAMARTPAR");
		obxArtPar->getOBX_5()->setData(std::to_string(sen_roj.getparentesis()));
		segToPipe(obxArtPar, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();


		HL7_24::OBX * obxArtSig= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxArtSig=getOBXHl(obxArtSig, BaseDB::ga.getPropertyConfig("LLAVE.ROJA_AMARILLA"), BaseDB::ga.getPropertyConfig("CODIG.ROJA_AMARILLA"));
		obxArtSig->getObservationSubId()->setData("RJAMARTSIGN");
		for (sigart = vectoart.begin(); sigart < vectoart.end(); sigart++)
		{
			ss << std::hex << *sigart;
		}
		obxArtSig->getOBX_5()->setData(ss.str());
		segToPipe(obxArtSig, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		vectoart.clear();
		//cleanOBX();
		

		ss.clear();

		sen_roj.backEstad();
		sen_roj.loadSAP(&mon);

		std::vector<uint8_t> vectoap = sen_roj.getSignal();
		std::vector<uint8_t>::iterator sigap = vectoap.begin();
		std::stringstream ss2;

		HL7_24::OBX * obxApMax= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxApMax=getOBXHl(obxApMax, BaseDB::ga.getPropertyConfig("LLAVE.ROJA_AMARILLA"), BaseDB::ga.getPropertyConfig("CODIG.ROJA_AMARILLA"));
		obxApMax->getObservationSubId()->setData("RJAMAPMAX");
		obxApMax->getOBX_5()->setData(std::to_string(sen_roj.getMax()));
		segToPipe(obxApMax, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();

		HL7_24::OBX * obxApMin= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxApMin=getOBXHl(obxApMin, BaseDB::ga.getPropertyConfig("LLAVE.ROJA_AMARILLA"), BaseDB::ga.getPropertyConfig("CODIG.ROJA_AMARILLA"));
		obxApMin->getObservationSubId()->setData("RJAMAPMIN");
		obxApMin->getOBX_5()->setData(std::to_string(sen_roj.getMin()));
		segToPipe(obxApMin, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();

		HL7_24::OBX * obxApPar= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxApPar=getOBXHl(obxApPar, BaseDB::ga.getPropertyConfig("LLAVE.ROJA_AMARILLA"), BaseDB::ga.getPropertyConfig("CODIG.ROJA_AMARILLA"));
		obxApPar->getObservationSubId()->setData("RJAMAPPAR");
		obxApPar->getOBX_5()->setData(std::to_string(sen_roj.getparentesis()));
		segToPipe(obxApPar, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();

		HL7_24::OBX * obxAptSig= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxAptSig=getOBXHl(obxAptSig, BaseDB::ga.getPropertyConfig("LLAVE.ROJA_AMARILLA"), BaseDB::ga.getPropertyConfig("CODIG.ROJA_AMARILLA"));
		obxAptSig->getObservationSubId()->setData("RJAMAPSIGN");
		char buf[3];
		buf[0]=0, buf[1]=0, buf[2]=0;
		std::string s;
		while (!vectoap.empty())
		{
			sprintf_s(buf, "%02x", vectoap.front());
			s.append(buf);
			//printf("%X", vectoap.front());
			buf[0] = 0, buf[1] = 0, buf[2] = 0;
			vectoap.erase(vectoap.begin());		
		}
		obxAptSig->getOBX_5()->setData(s);
		segToPipe(obxAptSig, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		vectoap.clear();
		ss2.clear();
		MSG.clear();
		//cleanOBX();
		
		return out;
	}
	catch (HL7Exception &ex) 
	{
		LOGW("SE LANZO UNA EXCEPCIOn cargando OBX signRojaAmar" << ex.what());
		throw ex;
	}

}

std::string MapeadorHl7::loadOBXFrecResp()
{
	try {
		HL7_24::ORU_R01 MSG;
		std::string out;
		std::string load;
		HL7_24::OBX * obxFRSig= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		std::stringstream ss;
		std::vector<uint8_t>::iterator itefr;
		std::vector<uint8_t> vecto = frec.getSigna();
		
		obxFRSig=getOBXHl(obxFRSig, BaseDB::ga.getPropertyConfig("LLAVE.FREC_RESP"), BaseDB::ga.getPropertyConfig("CODIG.FREC_RESP"));
		obxFRSig->getObservationSubId()->setData("FRSIG");
		

		for (itefr = vecto.begin(); itefr < vecto.end(); itefr++)
		{
			ss << std::hex << *itefr;
		}
		obxFRSig->getOBX_5()->setData(ss.str());
		segToPipe(obxFRSig, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();


		HL7_24::OBX * obxFRInd= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxFRInd=getOBXHl(obxFRInd, BaseDB::ga.getPropertyConfig("LLAVE.FREC_RESP"), BaseDB::ga.getPropertyConfig("CODIG.FREC_RESP"));
		obxFRInd->getObservationSubId()->setData("FRIMP");
		obxFRInd->getOBX_5()->setData(std::to_string(frec.getInpedancia()));	
		segToPipe(obxFRInd, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();
		MSG.clear();
		return out;
	}
	catch (HL7Exception &ex) {
		LOGW("[OBX1] SE LANZO UNA EXCEPCIOn cargando OBX FRECUENCIA RESPIRATORIA" << ex.what());
		throw ex;
	}
}

std::string MapeadorHl7::loadOBXTemp()
{
	try 
	{
		HL7_24::ORU_R01 MSG;
		std::string out;
		std::string load;
		HL7_24::OBX * obxOBXtemp1= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxOBXtemp1 =getOBXHl(obxOBXtemp1, BaseDB::ga.getPropertyConfig("LLAVE.TEMP"), BaseDB::ga.getPropertyConfig("CODIG.TEMP"));
		obxOBXtemp1->getObservationSubId()->setData("TEMPT1");
		obxOBXtemp1->getOBX_5()->setData(std::to_string(temp.getT1()));
		segToPipe(obxOBXtemp1, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();


		HL7_24::OBX * obxOBXtemp2= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxOBXtemp2 =getOBXHl(obxOBXtemp2, BaseDB::ga.getPropertyConfig("LLAVE.TEMP"), BaseDB::ga.getPropertyConfig("CODIG.TEMP"));
		obxOBXtemp2->getObservationSubId()->setData("TEMPT2");
		obxOBXtemp2->getOBX_5()->setData(std::to_string(temp.getT2()));
		segToPipe(obxOBXtemp2, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();

		HL7_24::OBX * obxOBXtemp3= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxOBXtemp3 =getOBXHl(obxOBXtemp3, BaseDB::ga.getPropertyConfig("LLAVE.TEMP"), BaseDB::ga.getPropertyConfig("CODIG.TEMP"));
		obxOBXtemp3->getObservationSubId()->setData("TEMPT3");
		obxOBXtemp3->getOBX_5()->setData(std::to_string(temp.getT3()));
		segToPipe(obxOBXtemp3, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();
		MSG.clear();

		return out;
		
	}
	catch (HL7Exception &ex)
	{
		LOGW("[TEMP] SE LANZO UNA EXCEPCIOn cargando OBX TEMOP" << ex.what());
		throw ex;
	}


}

std::vector<std::string> MapeadorHl7::loadOBXAlarm()
{
	std::string load;
	std::string out;
	std::string out2;
	std::vector<std::string> vectOut;
 	std::stringstream ss;
	std::vector<uint8_t>::iterator iteecg1;
	std::vector<uint8_t>::iterator iteecg2;
	std::vector<uint8_t>::iterator iteecg3;
	std::vector<uint8_t>ecgtem1 = ecg.getEcg1();
	std::vector<uint8_t>ecgtem2 = ecg.getEcg2();
	std::vector<uint8_t>ecgtem3 = ecg.getEcg3();
	HL7_24::ORU_R01 MSG;
	try {
		HL7_24::OBX * obxavf= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxavf =getOBXHl(obxavf, BaseDB::ga.getPropertyConfig("LLAVE.ALARM"), BaseDB::ga.getPropertyConfig("CODIG.ALARM"));
		obxavf->getObservationSubId()->setData("ALARMAVF");
		obxavf->getOBX_5()->setData(std::to_string(ecg.getaVF()));
		segToPipe(obxavf, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();


		HL7_24::OBX * obxavl= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxavl =getOBXHl(obxavl, BaseDB::ga.getPropertyConfig("LLAVE.ALARM"), BaseDB::ga.getPropertyConfig("CODIG.ALARM"));
		obxavl->getObservationSubId()->setData("ALARMAVL");
		obxavl->getOBX_5()->setData(std::to_string(ecg.getAvl()));
		segToPipe(obxavl, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();

		HL7_24::OBX * obxAvr= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxAvr =getOBXHl(obxAvr, BaseDB::ga.getPropertyConfig("LLAVE.ALARM"), BaseDB::ga.getPropertyConfig("CODIG.ALARM"));
		obxAvr->getObservationSubId()->setData("ALARMAVR");
		obxAvr->getOBX_5()->setData(std::to_string(ecg.getAvr()));
		segToPipe(obxAvr, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();

		HL7_24::OBX * obxCVP= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxCVP=getOBXHl(obxCVP, BaseDB::ga.getPropertyConfig("LLAVE.ALARM"), BaseDB::ga.getPropertyConfig("CODIG.ALARM"));
		obxCVP->getObservationSubId()->setData("ALARMCVP");
		obxCVP->getOBX_5()->setData(std::to_string(ecg.getCVP()));
		segToPipe(obxCVP, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();


		HL7_24::OBX  * obxFrec_Cardi= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxFrec_Cardi=getOBXHl(obxFrec_Cardi, BaseDB::ga.getPropertyConfig("LLAVE.ALARM"), BaseDB::ga.getPropertyConfig("CODIG.ALARM"));
		obxFrec_Cardi->getObservationSubId()->setData("ALARMECGFR");
		obxFrec_Cardi->getOBX_5()->setData(std::to_string(ecg.getFrec_Cardi()));
		segToPipe(obxFrec_Cardi ,load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();

		HL7_24::OBX * obxI= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxI=getOBXHl(obxI, BaseDB::ga.getPropertyConfig("LLAVE.ALARM"), BaseDB::ga.getPropertyConfig("CODIG.ALARM"));
		obxI->getObservationSubId()->setData("ALARMECGI");
		obxI->getOBX_5()->setData(std::to_string(ecg.getI()));
		segToPipe(obxI, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();

		HL7_24::OBX * obxII= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxII=getOBXHl(obxII, BaseDB::ga.getPropertyConfig("LLAVE.ALARM"), BaseDB::ga.getPropertyConfig("CODIG.ALARM"));
		obxII->getObservationSubId()->setData("ALARMECGII");
		obxII->getOBX_5()->setData(std::to_string(ecg.getII()));
		segToPipe(obxII, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();


		HL7_24::OBX * obxIII= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxIII=getOBXHl(obxIII, BaseDB::ga.getPropertyConfig("LLAVE.ALARM"), BaseDB::ga.getPropertyConfig("CODIG.ALARM"));
		obxIII->getObservationSubId()->setData("ALARMECGIII");
		obxIII->getOBX_5()->setData(std::to_string(ecg.getIII()));
		segToPipe(obxIII, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();

		HL7_24::OBX * obxV= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxV=getOBXHl(obxV, BaseDB::ga.getPropertyConfig("LLAVE.ALARM"), BaseDB::ga.getPropertyConfig("CODIG.ALARM"));
		obxV->getObservationSubId()->setData("ALARMECGV");
		obxV->getOBX_5()->setData(std::to_string(ecg.getV()));
		segToPipe(obxV, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();
		

		//a1 << std::hex << v2;


		HL7_24::OBX * obxECG1= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxECG1=getOBXHl(obxECG1, BaseDB::ga.getPropertyConfig("LLAVE.ALARM"), BaseDB::ga.getPropertyConfig("CODIG.ALARM"));
		obxECG1->getObservationSubId()->setData("ALARMECGECG1");
		for (iteecg1 = ecgtem1.begin(); iteecg1 < ecgtem1.end(); iteecg1++)
		{
			ss << std::hex << *iteecg1;
		}
		obxECG1->getOBX_5()->setData(ss.str());
		segToPipe(obxECG1, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();
		ss.clear();

		ss.str(std::string());
		ss.clear();
		HL7_24::OBX * obxECG2= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxECG2=getOBXHl(obxECG2, BaseDB::ga.getPropertyConfig("LLAVE.ALARM"), BaseDB::ga.getPropertyConfig("CODIG.ALARM"));
		obxECG2->getObservationSubId()->setData("ALARMECGECG2");
		for (iteecg2 = ecgtem2.begin(); iteecg2 < ecgtem2.end(); iteecg2++)
		{
			ss << std::hex << *iteecg2;
		}
		obxECG2->getOBX_5()->setData(ss.str());
		segToPipe(obxECG2, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();
		ss.clear();

		ss.str(std::string());
		ss.clear();
		HL7_24::OBX * obxECG3= MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxECG3=getOBXHl(obxECG3, BaseDB::ga.getPropertyConfig("LLAVE.ALARM"), BaseDB::ga.getPropertyConfig("CODIG.ALARM"));
		obxECG3->getObservationSubId()->setData("ALARMECGECG3");
		for (iteecg3 = ecgtem3.begin(); iteecg3 < ecgtem3.end(); iteecg3++)
		{
			ss << std::hex << *iteecg3;
		}
		obxECG3->getOBX_5()->setData(ss.str());
		segToPipe(obxECG3, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();
		ss.clear();

		//volver a generar el de alarma
		if (BaseDB::ga.sizeFile1(ecg.getdirecc() + mon.getIp() + "\\ALARMAMEN.txt") > 0)
		{
			alrm.loadMens(mon.getIp() + "\\ALARMAMEN.txt");
			for (int i = 0; i < alrm.getMensajes().size(); i++)
			{
				alrm.setAlarmSeverity(alrm.contChar(alrm.getMensajes().at(i)));
				alrm.setDescription(alrm.getMensajes().at(i));

				HL7_24::OBX * obxAlarmMesa= MSG.getPATIENT_RESULT()->
					getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();

				obxAlarmMesa->getOBX_1()->setData(std::string(""));
				obxAlarmMesa->getOBX_2()->setData(BaseDB::ga.getPropertyConfig("OBX.ALARM"));
				obxAlarmMesa->getOBX_3()->getCE_1()->setData(BaseDB::ga.getPropertyConfig("CODIG.ALARM"));
				obxAlarmMesa->getOBX_3()->getCE_2()->setData(BaseDB::ga.getPropertyConfig("LLAVE.ALARM"));
				obxAlarmMesa->getOBX_4()->setData(std::to_string(alrm.getAlarmSeverity()));
				obxAlarmMesa->getOBX_5()->setData(alrm.getDescription());
				obxAlarmMesa->getOBX_6()->getCE_1()->setData(std::string(""));
				obxAlarmMesa->getOBX_7()->setData(std::string(""));
				obxAlarmMesa->getOBX_8()->setData(std::string(""));
				obxAlarmMesa->getOBX_9()->setData(std::string(""));
				obxAlarmMesa->getOBX_10()->setData(std::string(""));
				obxAlarmMesa->getOBX_11()->setData(std::string("F"));
				obxAlarmMesa->getOBX_12()->getTS_1()->setData(std::string(""));
				obxAlarmMesa->getOBX_13()->setName("PHY_ALM");
				obxAlarmMesa->getOBX_14()->getTS_1()->setData(date_time);
				segToPipe(obxAlarmMesa,load);
				load.insert(0, "<VT>");
				load.append("<CR>");
				out2=out2.append(load);
				load.clear();
				load.swap(std::string());
				
				//cleanOBX();
			}
			vectOut.push_back(out2);
		}
		vectOut.push_back(out);
		MSG.clear();
		return vectOut;
	}
	catch (HL7Exception &ex) {
		LOGW("[ALERAT] SE PRESENTO UN EXCEPCION EN LA GENERACION DE ALERTA" << ex.what());
		throw ex;
	}
}



std::vector<std::string> MapeadorHl7::loadOBXAlarSock()
{
	std::string load;
	std::string out;
	std::string out2;
	std::vector<std::string> vectOut;
	std::stringstream ss;
	std::vector<uint8_t>::iterator iteecg1;
	std::vector<uint8_t>::iterator iteecg2;
	std::vector<uint8_t>::iterator iteecg3;
	std::vector<uint8_t>ecgtem1 = ecg.getEcg1();
	std::vector<uint8_t>ecgtem2 = ecg.getEcg2();
	std::vector<uint8_t>ecgtem3 = ecg.getEcg3();
	std::string end = "@end@";
	HL7_24::ORU_R01 MSG;
	try {
		HL7_24::OBX * obxavf = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxavf = getOBXHl(obxavf, BaseDB::ga.getPropertyConfig("LLAVE.ALARM"), BaseDB::ga.getPropertyConfig("CODIG.ALARM"));
		obxavf->getObservationSubId()->setData("ALARMAVF");
		obxavf->getOBX_5()->setData(std::to_string(ecg.getaVF()));
		segToPipe(obxavf, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();


		HL7_24::OBX * obxavl = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxavl = getOBXHl(obxavl, BaseDB::ga.getPropertyConfig("LLAVE.ALARM"), BaseDB::ga.getPropertyConfig("CODIG.ALARM"));
		obxavl->getObservationSubId()->setData("ALARMAVL");
		obxavl->getOBX_5()->setData(std::to_string(ecg.getAvl()));
		segToPipe(obxavl, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();

		HL7_24::OBX * obxAvr = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxAvr = getOBXHl(obxAvr, BaseDB::ga.getPropertyConfig("LLAVE.ALARM"), BaseDB::ga.getPropertyConfig("CODIG.ALARM"));
		obxAvr->getObservationSubId()->setData("ALARMAVR");
		obxAvr->getOBX_5()->setData(std::to_string(ecg.getAvr()));
		segToPipe(obxAvr, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();

		HL7_24::OBX * obxCVP = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxCVP = getOBXHl(obxCVP, BaseDB::ga.getPropertyConfig("LLAVE.ALARM"), BaseDB::ga.getPropertyConfig("CODIG.ALARM"));
		obxCVP->getObservationSubId()->setData("ALARMCVP");
		obxCVP->getOBX_5()->setData(std::to_string(ecg.getCVP()));
		segToPipe(obxCVP, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();


		HL7_24::OBX  * obxFrec_Cardi = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxFrec_Cardi = getOBXHl(obxFrec_Cardi, BaseDB::ga.getPropertyConfig("LLAVE.ALARM"), BaseDB::ga.getPropertyConfig("CODIG.ALARM"));
		obxFrec_Cardi->getObservationSubId()->setData("ALARMECGFR");
		obxFrec_Cardi->getOBX_5()->setData(std::to_string(ecg.getFrec_Cardi()));
		segToPipe(obxFrec_Cardi, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();

		HL7_24::OBX * obxI = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxI = getOBXHl(obxI, BaseDB::ga.getPropertyConfig("LLAVE.ALARM"), BaseDB::ga.getPropertyConfig("CODIG.ALARM"));
		obxI->getObservationSubId()->setData("ALARMECGI");
		obxI->getOBX_5()->setData(std::to_string(ecg.getI()));
		segToPipe(obxI, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();

		HL7_24::OBX * obxII = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxII = getOBXHl(obxII, BaseDB::ga.getPropertyConfig("LLAVE.ALARM"), BaseDB::ga.getPropertyConfig("CODIG.ALARM"));
		obxII->getObservationSubId()->setData("ALARMECGII");
		obxII->getOBX_5()->setData(std::to_string(ecg.getII()));
		segToPipe(obxII, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();


		HL7_24::OBX * obxIII = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxIII = getOBXHl(obxIII, BaseDB::ga.getPropertyConfig("LLAVE.ALARM"), BaseDB::ga.getPropertyConfig("CODIG.ALARM"));
		obxIII->getObservationSubId()->setData("ALARMECGIII");
		obxIII->getOBX_5()->setData(std::to_string(ecg.getIII()));
		segToPipe(obxIII, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();

		HL7_24::OBX * obxV = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxV = getOBXHl(obxV, BaseDB::ga.getPropertyConfig("LLAVE.ALARM"), BaseDB::ga.getPropertyConfig("CODIG.ALARM"));
		obxV->getObservationSubId()->setData("ALARMECGV");
		obxV->getOBX_5()->setData(std::to_string(ecg.getV()));
		segToPipe(obxV, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();


		//a1 << std::hex << v2;


		HL7_24::OBX * obxECG1 = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		std::string out5;
		obxECG1 = getOBXHl(obxECG1, BaseDB::ga.getPropertyConfig("LLAVE.ALARM"), BaseDB::ga.getPropertyConfig("CODIG.ALARM"));
		obxECG1->getObservationSubId()->setData("ALARMECGECG1");
		for (iteecg1 = ecgtem1.begin(); iteecg1 < ecgtem1.end(); iteecg1++)
		{
			out5 = out5 +std::to_string(*iteecg1) +"^";
		}
		obxECG1->getOBX_5()->setData(out5);
		segToPipe(obxECG1, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();


		out5.clear();
		out5 = std::string();
		HL7_24::OBX * obxECG2 = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxECG2 = getOBXHl(obxECG2, BaseDB::ga.getPropertyConfig("LLAVE.ALARM"), BaseDB::ga.getPropertyConfig("CODIG.ALARM"));
		obxECG2->getObservationSubId()->setData("ALARMECGECG2");
		for (iteecg2 = ecgtem2.begin(); iteecg2 < ecgtem2.end(); iteecg2++)
		{
			out5 = out5 + std::to_string(*iteecg2)+"^";
		}
		obxECG2->getOBX_5()->setData(out5);
		segToPipe(obxECG2, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();
		

		out5.clear();
		out5 = std::string();
		HL7_24::OBX * obxECG3 = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
		obxECG3 = getOBXHl(obxECG3, BaseDB::ga.getPropertyConfig("LLAVE.ALARM"), BaseDB::ga.getPropertyConfig("CODIG.ALARM"));
		obxECG3->getObservationSubId()->setData("ALARMECGECG3");
		for (iteecg3 = ecgtem3.begin(); iteecg3 < ecgtem3.end(); iteecg3++)
		{
			out5=out5 +std::to_string(*iteecg3)+"^";
		}
		obxECG3->getOBX_5()->setData(out5);
		segToPipe(obxECG3, load);
		load.insert(0, "<VT>");
		load.append("<CR>");
		load.append(end);
		out.append(load);
		load.clear();
		load.swap(std::string());
		//cleanOBX();
		out5.clear();
		out5 = std::string();

		//volver a generar el de alarma
		if (BaseDB::ga.sizeFile1(ecg.getdirecc() + mon.getIp() + "\\ALARMAMEN.txt") > 0)
		{
			alrm.loadMens(mon.getIp() + "\\ALARMAMEN.txt");
			for (int i = 0; i < alrm.getMensajes().size(); i++)
			{
				alrm.setAlarmSeverity(alrm.contChar(alrm.getMensajes().at(i)));
				alrm.setDescription(alrm.getMensajes().at(i));

				HL7_24::OBX * obxAlarmMesa = MSG.getPATIENT_RESULT()->
					getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();

				obxAlarmMesa->getOBX_1()->setData(std::string(""));
				obxAlarmMesa->getOBX_2()->setData(BaseDB::ga.getPropertyConfig("OBX.ALARM"));
				obxAlarmMesa->getOBX_3()->getCE_1()->setData(BaseDB::ga.getPropertyConfig("CODIG.ALARM"));
				obxAlarmMesa->getOBX_3()->getCE_2()->setData(BaseDB::ga.getPropertyConfig("LLAVE.ALARM"));
				obxAlarmMesa->getOBX_4()->setData(std::to_string(alrm.getAlarmSeverity()));
				obxAlarmMesa->getOBX_5()->setData(alrm.getDescription());
				obxAlarmMesa->getOBX_6()->getCE_1()->setData(std::string(""));
				obxAlarmMesa->getOBX_7()->setData(std::string(""));
				obxAlarmMesa->getOBX_8()->setData(std::string(""));
				obxAlarmMesa->getOBX_9()->setData(std::string(""));
				obxAlarmMesa->getOBX_10()->setData(std::string(""));
				obxAlarmMesa->getOBX_11()->setData(std::string("F"));
				obxAlarmMesa->getOBX_12()->getTS_1()->setData(std::string(""));
				obxAlarmMesa->getOBX_13()->setName("PHY_ALM");
				obxAlarmMesa->getOBX_14()->getTS_1()->setData(date_time);
				segToPipe(obxAlarmMesa, load);
				load.insert(0, "<VT>");
				load.append("<CR>");
				out2 = out2.append(load);
				load.clear();
				load.swap(std::string());

				//cleanOBX();
			}
			vectOut.push_back(out2);
		}
		vectOut.push_back(out);
		MSG.clear();
		return vectOut;
	}
	catch (HL7Exception &ex) {
		LOGW("[ALERAT] SE PRESENTO UN EXCEPCION EN LA GENERACION DE ALERTA" << ex.what());
		throw ex;
	}
}



std::string MapeadorHl7::generate_hex(const unsigned int len)
{
	try {
		std::stringstream ss;
		for (auto i = 0; i < len; i++) {
			const auto rc = random_char();
			std::stringstream hexstream;
			hexstream << std::hex << rc;
			auto hex = hexstream.str();
			ss << (hex.length() < 2 ? '0' + hex : hex);
		}
		return ss.str();
	}
	catch (std::ifstream::failure &fe ) 
	{
		
		LOGW("SE genero un error generando el UUID"<<fe.what());
		throw &fe;
	}
}

std::string MapeadorHl7::UUID4() 
{
	try {
		std::string uuid(generate_hex(8));
		uuid = uuid.append("-");
		uuid = uuid.append(generate_hex(4));
		uuid = uuid.append("-");
		uuid = uuid.append("4" + generate_hex(3));
		uuid = uuid.append("-");
		uuid = uuid.append("1" + generate_hex(3));
		uuid = uuid.append("-");
		uuid = uuid.append(generate_hex(12));
		return uuid;
	}
	catch (std::ifstream::failure &fa)
	{
		LOGW("SE returno un UUUID gnull");
		return "00000000-0000-4000-1000-000000000";
	}
}


void MapeadorHl7::clean() 
{
	ecg.backEstad();
	spo2.backEstad();
	frec.backEstad();
	sen_roj.backEstad();
	temp.backEstad();
	alrm.clearMensajes();
	/*
	MSG.getPATIENT_RESULT()->
		getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40()->clear();
	MSG.getPATIENT_RESULT()->
		getORDER_OBSERVATION()->getOBSERVATION()->getNTE_44()->clear();

	MSG.getPATIENT_RESULT()->
		getORDER_OBSERVATION()->getOBSERVATION()->clear();

	MSG.getPATIENT_RESULT()->
		getORDER_OBSERVATION()->getCTD_36()->clear();
	MSG.getPATIENT_RESULT()->
		getORDER_OBSERVATION()->getCTI_55()->clear();
	MSG.getPATIENT_RESULT()->
		getORDER_OBSERVATION()->getOBR_29()->clear();
	
	MSG.getPATIENT_RESULT()->
		getPATIENT()->
		getVISIT()->getPV1_19()->clear();


	MSG.getPATIENT_RESULT()->
		getPATIENT()->
		getVISIT()->clear();

	MSG.getPATIENT_RESULT()->
		getPATIENT()->clear();
	MSG.getPATIENT_RESULT()->clear();*/
	//MSG.getMSH_1()->clear();
	//cleanMSH();
	//cleanPID();
	//cleanPV1();
	//cleanoOBR();
	//cleanOBX();
	//MSG.clear();
	map.clear();
}


void MapeadorHl7::setDate(const std::string  &dat) {
	this->date_time = dat;
}



std::string MapeadorHl7::getDate() {
	return this->date_time;
}

void MapeadorHl7::loadFile() {
	BaseDB::ga.loadPropertis();
}

std::map<std::string, std::vector<std::string>> MapeadorHl7::getpipeOUT()
{
	//LOGA("[LOGOUTpipe] esta apunto de enviar "<< strout);
	return map;
}



std::vector<std::string> MapeadorHl7::getAlarm() 
{
	return loadOBXAlarSock();
}

/*
void MapeadorHl7::cleanMSH()
{
	
	HL7_24::MSH * msh = MSG.getMSH_1();
	msh->getMSH_2()->clear();
	msh->getMSH_3()->clear();
	msh->getMSH_3()->clear();
	msh->getMSH_3()->clear();
	msh->getMSH_4()->clear();
	msh->getMSH_4()->clear();
	msh->getMSH_4()->clear();
	msh->getMSH_5()->clear();
	msh->getMSH_5()->clear();
	msh->getMSH_5()->clear();
	msh->getMSH_6()->clear();
	msh->getMSH_6()->clear();
	msh->getMSH_6()->clear();
	msh->getMSH_7()->clear();
	msh->getMSH_8()->clear();
	msh->getMSH_9()->clear();
	msh->getMSH_10()->clear();
	msh->getMSH_11()->clear();
	msh->getMSH_12()->clear();
}

void MapeadorHl7::cleanPID()
{
	HL7_24::PID * Pid = MSG.getPATIENT_RESULT()->
		getPATIENT()->
		getPID_4();
	Pid->getPID_1()->clear();
	Pid->getPID_2()->getCX_1()->clear();
	Pid->getPID_3()->getCX_1()->clear();
	Pid->getPID_3()->getCX_2()->clear();
	Pid->getPID_4()->getCX_1()->clear();
	Pid->getPID_5()->getFamilyName()->getFN_1()->clear();
	Pid->getPID_6()->getXPN_1()->getFN_1()->clear();
	Pid->getPID_7()->getTS_1()->clear();
	Pid->getPID_8()->clear();
	Pid->getPID_9()->getXPN_1()->getFN_1()->clear();
	Pid->getPID_10()->getAlternateText()->clear();
	Pid->getPID_11()->getXAD_1()->getSAD_1()->clear();
	Pid->getPID_11()->getXAD_2()->clear();
	Pid->getPID_11()->getXAD_3()->clear();
	Pid->getPID_11()->getXAD_4()->clear();
	Pid->getPID_11()->getXAD_5()->clear();
	Pid->getPID_11()->getXAD_6()->clear();
	Pid->getPID_11()->getXAD_7()->clear();
	Pid->getPID_11()->getXAD_8()->clear();
	Pid->getPID_12()->clear();
	Pid->getPID_13()->getXTN_1()->clear();
	Pid->getPID_13()->getXTN_2()->clear();
	Pid->getPID_13()->getXTN_3()->clear();
	Pid->getPID_13()->getXTN_4()->clear();
	Pid->getPID_13()->getXTN_5()->clear();
	Pid->getPID_13()->getXTN_6()->clear();
	Pid->getPID_13()->getXTN_7()->clear();
	Pid->getPID_13()->getXTN_8()->clear();
	Pid->getPID_13()->getXTN_9()->clear();
}


void MapeadorHl7::cleanPV1()
{  
	HL7_24::PV1 * pv1 = MSG.getPATIENT_RESULT()->
		getPATIENT()->
		getVISIT()->
		getPV1_19();
	pv1->getPV1_1()->clear();
	pv1->getPV1_2()->clear();
	pv1->getPV1_3()->getBed()->clear();
	pv1->getPV1_4()->clear();
	pv1->getPV1_5()->getCX_1()->clear();
	pv1->getPV1_6()->getPL_1()->clear();
	pv1->getPV1_7()->getXCN_1()->clear();
	pv1->getPV1_7()->getXCN_2()->getFN_1()->clear();
	pv1->getPV1_7()->getXCN_3()->clear();
	pv1->getPV1_8()->getXCN_1()->clear();
	pv1->getPV1_9()->getXCN_1()->clear();
	pv1->getPV1_10()->clear();
	pv1->getPV1_11()->getPL_1()->clear();
	pv1->getPV1_12()->clear();
	pv1->getPV1_13()->clear();
	pv1->getPV1_14()->clear();
	pv1->getPV1_15()->clear();
	pv1->getPV1_16()->clear();
	pv1->getPV1_17()->getXCN_1()->clear();
	pv1->getPV1_18()->clear();
	pv1->getPV1_19()->getCX_1()->clear();
	pv1->getPV1_20()->getFC_1()->clear();
	pv1->getPV1_21()->clear();
	pv1->getPV1_22()->clear();
	pv1->getPV1_23()->clear();
	pv1->getPV1_24()->clear();
	pv1->getPV1_25()->clear();
	pv1->getPV1_26()->clear();
	pv1->getPV1_27()->clear();
	pv1->getPV1_28()->clear();
	pv1->getPV1_29()->clear();
	pv1->getPV1_30()->clear();
	pv1->getPV1_31()->clear();
	pv1->getPV1_32()->clear();
	pv1->getPV1_33()->clear();
	pv1->getPV1_34()->clear();
	pv1->getPV1_35()->clear();
	pv1->getPV1_36()->clear();
	pv1->getPV1_37()->getDLD_1()->clear();
	pv1->getPV1_38()->getAlternateText()->clear();
	pv1->getPV1_39()->clear();
	pv1->getPV1_40()->clear();
	pv1->getPV1_41()->clear();
	pv1->getPV1_42()->getPL_1()->clear();
	pv1->getPV1_43()->getPL_1()->clear();
	pv1->getPV1_44()->getTS_1()->clear();
}

void MapeadorHl7::cleanoOBR() {
	HL7_24::OBR * obr = MSG.getPATIENT_RESULT()->
		getORDER_OBSERVATION()->
		getOBR_29();
	obr->getOBR_1()->clear();
	obr->getOBR_2()->getEI_1()->clear();
	obr->getOBR_3()->getEI_1()->clear();
	obr->getOBR_4()->getCE_1()->clear();
	obr->getOBR_4()->getCE_2()->clear();
	obr->getOBR_4()->getCE_3()->clear();
	obr->getOBR_4()->getCE_4()->clear();
	obr->getOBR_4()->getAlternateText()->clear();
	obr->getOBR_5()->clear();
	obr->getOBR_6()->getTS_1()->clear();
	obr->getOBR_7()->getTS_1()->clear();
}


void MapeadorHl7::cleanOBX() 
{
	HL7_24::OBX * obxAlarmMesa = MSG.getPATIENT_RESULT()->
		getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40();
	obxAlarmMesa->getOBX_1()->clear();
	obxAlarmMesa->getOBX_2()->clear();
	obxAlarmMesa->getOBX_3()->getCE_1()->clear();
	obxAlarmMesa->getOBX_3()->getCE_2()->clear();
	obxAlarmMesa->getOBX_4()->clear();
	obxAlarmMesa->getOBX_5()->clear();
	obxAlarmMesa->getOBX_6()->getCE_1()->clear();
	obxAlarmMesa->getOBX_7()->clear();
	obxAlarmMesa->getOBX_8()->clear();
	obxAlarmMesa->getOBX_9()->clear();
	obxAlarmMesa->getOBX_10()->clear();
	obxAlarmMesa->getOBX_11()->clear();
	obxAlarmMesa->getOBX_12()->getTS_1()->clear();
	obxAlarmMesa->getOBX_13()->clear();
	obxAlarmMesa->getOBX_14()->getTS_1()->clear();
}*/