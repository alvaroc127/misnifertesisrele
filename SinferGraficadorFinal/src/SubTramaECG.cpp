#include "..\Include\SubTramaECG.h"




SubTramaECG::SubTramaECG(const SubTramaECG &) {

}

SubTramaECG::SubTramaECG()
{

}

SubTramaECG::SubTramaECG(uint8_t start[] , uint8_t vodi[], uint8_t size[]):SubTramaParam(start,vodi,size){
	
}


SubTramaECG::~SubTramaECG()
{
	
}

SubTramaECG::SubTramaECG(SubTramaParam * sub):SubTramaParam(sub) {

}

SubTramaECG::SubTramaECG(const SubTramaParam & sub ):SubTramaParam(sub) {

}

int SubTramaECG::runData(const std::vector<uint8_t> &datas,int pos) {
	uint32_t sal = datas.at(pos++);
	sal = sal << 16;
	sal = sal |datas.at(pos++);
	sal = sal << 8;
	sal = sal | datas.at(pos++);
	//uint32_t sal = (datas.at(pos++) << 16) | (datas.at(pos++) << 8) | (datas.at(pos++));
	this->val = (int)sal;
	return pos;
}




void SubTramaECG::clasficaSubTra(const std::vector<uint8_t> &datas,int pos) {
	bool ban = true;
	for (int j = pos; j < datas.size() && pos + 3 < datas.size() && ban == true;j++) {
		pos = runData(datas, pos);
		
		switch (this->val) {
		case(16777221):
			pos = loadFrecu(datas, pos);
			break;

		case(33554437):
			pos = loadCVP(datas, pos);
			break;

		case(83886602):
			pos = loadI(datas, pos);
			break;

		case(100663818):
			pos = loadII(datas, pos);
			break;

		case(117441034):
			pos = loadIII(datas, pos);
			break;

		case(134218250):
			pos = loadAVR(datas, pos);
			break;

		case(150995466):
			pos = loadAVL(datas, pos);
			break;

		case(167772682):
			pos = loadAVF(datas, pos);
			break;


		case(-922746358):
			pos = loadV(datas, pos);
			break;

		case(33573641):
			ban = false;
			break;
		}
	}
}



int SubTramaECG::loadFrecu(const std::vector<uint8_t> &datas, int pos) {
	uint16_t sal = datas.at(pos++);
	sal = sal << 8;
	sal = sal | datas.at(pos);
	//uint16_t sal = (datas.at(pos++) << 8) | (datas.at(pos));
	this->frecuencia = (int)sal;
	return ++pos;
}

int SubTramaECG::loadCVP(const std::vector<uint8_t> &datas,int pos) {
	uint16_t sal = datas.at(pos++);
	sal = sal << 8;
	uint8_t val = datas.at(pos);
	sal = sal | val;
	//uint16_t sal = (datas.at(pos++) << 8) | (datas.at(pos));
	this->CVP = (int)sal;
	if (this->CVP>32768) {
		CVP = (CVP - 65536) / 100;
	}
	else {
		CVP = CVP / 100;
	}
	return ++pos;
}



int SubTramaECG::loadAVR(const std::vector<uint8_t> &datas, int pos) {
	//uint16_t sal = (datas.at(pos++) << 8) | (datas.at(pos));
	uint16_t sal = datas.at(pos++);
	sal = sal << 8;
	uint8_t val = datas.at(pos);
	sal = sal | val;
	this->aVR = (int)sal;
	if (this->aVR>32768) {
		this->aVR = (aVR - 65536) / 100;
	}
	else {
		aVR = aVR / 100;
	}
	return ++pos;
}




int SubTramaECG::loadAVL(const std::vector<uint8_t> &datas, int pos) {
	//uint16_t sal = (datas.at(pos++) << 8) | (datas.at(pos));
	uint16_t sal = datas.at(pos++);
	sal = sal << 8;
	uint8_t val = datas.at(pos);
	sal = sal | val;
	this->aVL = (int)sal;
	
	this->aVL= (int)sal;
	if (this->aVL>32768) {
		aVL = (aVL - 65536) / 100;
	}
	else {
		aVL = aVL / 100;
	}
	return ++pos;
}


int SubTramaECG::loadAVF(const std::vector<uint8_t> &datas, int pos) {
	//uint16_t sal = (datas.at(pos++) << 8) | (datas.at(pos));
	uint16_t sal = datas.at(pos++);
	sal = sal << 8;
	uint8_t val = datas.at(pos);
	sal = sal | val;
	this->aVF = (int)sal;
	
	this->aVF = (int)sal;
	if (this->aVF>32768) {
		aVF = (aVF - 65536) / 100;
	}
	else {
		aVF = aVF / 100;
	}
	return ++pos;
}


int SubTramaECG::loadV(const std::vector<uint8_t> &datas, int pos) {
	//uint16_t sal = (datas.at(pos++) << 8) | (datas.at(pos));
	uint16_t sal = datas.at(pos++);
	sal = sal << 8;
	uint8_t val = datas.at(pos);
	sal = sal | val;
	this->V = (int)sal;

	if (this->V > 32768) {
		V = (V - 65536) / 100;
	}
	else {
		V = V / 100;
	}
	return ++pos;
}


int SubTramaECG::loadIII(const std::vector<uint8_t> &datas, int pos) {
	//uint16_t sal = (datas.at(pos++) << 8) | (datas.at(pos));
	uint16_t sal = datas.at(pos++);
	sal = sal << 8;
	uint8_t val = datas.at(pos);
	sal = sal | val;
	this->III = (int)sal;
	if (this->III>32768) {
		III = (III - 65536) / 100;
	}
	else {
		III = III / 100;
	}
	return ++pos;
}


int SubTramaECG::loadII(const std::vector<uint8_t> &datas, int pos) {
	uint16_t sal = datas.at(pos++);
	sal = sal << 8;
	uint8_t val = datas.at(pos);
	sal = sal | val;
	//uint16_t sal = (datas.at(pos++) << 8) | (datas.at(pos));
	this->II = (int)sal;
	if (this->II>32768) {
		II = (II - 65536) / 100;
	}
	else {
		II = II / 100;
	}
	return ++pos;
}


int SubTramaECG::loadI(const std::vector<uint8_t> &datas, int pos) {
	//uint16_t sal = (datas.at(pos++) << 8) | (datas.at(pos));
	uint16_t sal = datas.at(pos++);
	sal = sal << 8;
	uint8_t val = datas.at(pos);
	sal = sal | val;
	this->I = (int)sal;
	if (this->I>32768) {
		I = (I - 65536) / 100;
	}
	else {
		I = I / 100;
	}
	return ++pos;
}


ECG SubTramaECG::datTram(ECG & tip) {
	 tip.aVF=this->aVF;
	 tip.aVL=this->aVL;
	 tip.aVR=this->aVR;
	 tip.CVP=this->CVP;
	tip.frecuencia=this->frecuencia;
	tip.I=this->I;
	tip.V=this->V;
	 tip.II=this->II;
	 tip.III=this->III;
	 tip.tipo = "ECGPARAM.txt";
	 return tip;
}

double SubTramaECG::getAVF() {
	return this->aVF;
}

double SubTramaECG::getAVL() {
	return this->aVL;
}

double SubTramaECG::getAVR() {
	return this->aVR;
}

double SubTramaECG::getCVP() {
	return this->CVP;
}

double SubTramaECG::getFrecuen() {
	return this->frecuencia;
}

double SubTramaECG::getI() {
	return this->I;
}

double SubTramaECG::getII() {
	return this->II;
}
double SubTramaECG::getV() {
		return V;
}

double SubTramaECG::getIII() {
	return this->III;
}

