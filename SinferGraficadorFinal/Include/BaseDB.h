#if !defined (_BASEDB_)
#pragma once

#include <vector>
#include <../Include/GestorArchivo.h>


/// <summary>
/// interface for DBclass
/// </summary>
class BaseDB
{
	public:
		 static GestorArchivo ga;
		BaseDB() {}

		virtual ~BaseDB() {}

	
	/// <summary>
	/// Reads the file sig.
	/// </summary>
	/// <param name="ip">The ip.</param>
	/// <returns></returns>
	virtual std::vector<uint8_t> readFileSig(const std::string &ip) = 0;
	
	/// <summary>
	/// Reads the file parameter.
	/// </summary>
	/// <param name="">The .</param>
	virtual void readFileParam(const std::string &) = 0;
	
	/// <summary>
	/// Backs the state of class 
	/// </summary>
	virtual void backEstad() = 0;


};

#endif
