/*
 * This file is part of Auriga HL7 library.
 *
 * Auriga HL7 library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Auriga HL7 library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Auriga HL7 library.  If not, see <http://www.gnu.org/licenses/>.
 */

#if !defined(_MLLPH_)
#define _MLLPH_
#pragma once 
#include <../Include/comunicacion/hl7socket.h>
#include <log4z.h>

class HL7MLLP : public  HL7Socket
{
public:

	HL7MLLP(const std::string &host, const std::string &port);

	HL7MLLP(int sockfd);

	HL7MLLP();

	~HL7MLLP();

	bool isInited();

	size_t
		send_msg_mllp( std::string& data);


	size_t
		read_msg_mllp(std::string& data);

	void closeS();

//#ifdef TESTS
//     void setSocket( int new_sock ){ HL7Socket::setSocket( new_sock ); }
//#endif // TESTS
};

#endif