#if !defined(_SOCKETCONECT_)
#define _SOCKETCONECT_
//#define TINS_STATIC
#pragma once

//#include <tins\tins.h>
#include <../Include/comunicacion/hl7mllp.h>
#include <log4z.h>
#include <../Include/BaseDB.h>


class SocketConect
{
	private:
		//Tins::NetworkInterface inter;
		HL7MLLP sock;
		bool conect = false;
		
	public:
				
		/// <summary>
		/// Initializes a new instance of the <see cref="SocketConect"/> class.
		/// </summary>
		SocketConect();
		
		/// <summary>
		/// Finalizes an instance of the <see cref="SocketConect"/> class.
		/// </summary>
		virtual ~SocketConect();
		
		/// <summary>
		/// Sets the interface.
		/// </summary>
		/// <param name="net">The network interface</param>
		//void setInterface(const Tins::NetworkInterface&);
		void setInterface();
		
		/// <summary>
		/// This method is necesary for generate the comunication
		/// </summary>
		/// <returns></returns>
		void sendandRecived( std::string &);

		
		/// <summary>
		/// Sendands the recived.
		/// </summary>
		/// <param name="stream">The stream.</param>
		/// <param name="ip">The ip.</param>
		/// <param name="port">The port.</param>
		void sendandRecived(std::string &stream, std::string &port);

};

#endif