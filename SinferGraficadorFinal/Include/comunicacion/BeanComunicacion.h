#if !defined(_BEANCOMUN_)
#define _BEANCOMUN_

#pragma once


#include "MapeadorHl7.h"
#include "SocketConect.h"


class BeanComunicacion 
{
	private:
		MapeadorHl7 mapper;
		SocketConect sock;
		SocketConect alarsock;
		GestorArchivo *ga;
		std::vector<std::string> vec;

		void appendVector(const std::map<std::string, std::vector<std::string>> &);

		void sendMensaje();

	public:	
				
		/// <summary>
		/// Setgs the ga.
		/// </summary>
		void setgGa(GestorArchivo *);
		
		/// <summary>
		/// Gets the ga.
		/// </summary>
		/// <returns></returns>
		GestorArchivo * getGa();

		/// <summary>
		/// Initializes a new instance of the <see cref="BeanComunicacion"/> class.
		/// Construcotr of bean
		/// </summary>
		BeanComunicacion();
		
		/// <summary>
		///  Destructor of class 
		/// Finalizes an instance of the <see cref="BeanComunicacion"/> class.
		/// </summary>
		virtual ~BeanComunicacion();
		
		/// <summary>
		/// Sends the inft.
		/// </summary>
		void createMsgHl7(const std::string & ip, const std::string &date_time);

		
		/// <summary>
		/// Loads the file properties for load propertiesin the sources
		/// </summary>
		void loadFile();

		
		/// <summary>
		/// Loads the interface oft network
		/// </summary>
		//void setInterface(const Tins::NetworkInterface&);
		void setInterface();
		
		/// <summary>
		/// Procalarms this instance.
		/// </summary>
		void procalarm();
		

};
#endif


