#if !defined (_MAPHL7_)
#define _MAPHL7_
#pragma once

#include <log4z.h>
#include <random>
#include <2.4/message/ORU_R01.h>
#include <common/ObjToPipe.h>
#include <../Include/AlarmaBD.h>
#include <../Include/Ecg.h>
#include <../Include/SPO2BD.h>
#include <../Include/frec_respiratoriDB.h>
#include <../Include/Monitor1.h>
#include <../Include/Senal_Roja_Amarilla.h>
#include <../Include/temperatura.h>
#include <../Include/BaseDB.h>
#include <../Include/TemperaturaDB.h>
#include <../Include/GestorArchivo.h>




/// <summary>
/// Represent the class for generate Hl7 Messages
/// </summary>
class MapeadorHl7
{
private:
	std::string date_time;

	Ecg ecg;
	AlarmaBD alrm;
	Monitor1 mon;
	frec_respiratoriDB frec;
	Senal_Roja_Amarilla sen_roj;
	SPO2BD spo2;
	TemperaturaDB temp;
	GestorArchivo * ga;
	std::string strout;
	std::map<std::string, std::vector<std::string>>map;

	
	/// <summary>
	/// Loadfiles the data.
	/// </summary>
	/// <param name="">The .</param>
	void loadfileData(const std::string &);

	/// <summary>
	/// Randoms the character. Generate UUID
	/// </summary>
	/// <returns></returns>
	unsigned int random_char();


	/// <summary>
		/// Generates the hexadecimal. string
		/// </summary>
		/// <param name="len">The length.</param>
		/// <returns></returns>
	std::string generate_hex(const unsigned int len);
	

	/// <summary>
	/// Loads the obxecg.
	/// </summary>
	/// <param name="">The references to OBX segment .</param>
	std::string  loadOBXECG();


	/// <summary>
	/// Loads the obxsPO2.
	/// </summary>
	/// <param name="">The references to OBX segment .</param>
	std::string loadOBXSPO2();

	/// <summary>
	/// Loads the obxSigRojAM
	/// </summary>
	/// <param name="">The references to OBX segment .</param>
	std::string loadOBXSiglRojaAmr();


	/// <summary>
	/// Loads the obxFrecResp.
	/// </summary>
	/// <param name="">The references to OBX segment .</param>
	std::string loadOBXFrecResp();


	/// <summary>
	/// Loads the obxTEmp
	/// </summary>
	/// <param name="">The references to OBX segment .</param>
	std::string  loadOBXTemp();


	/// <summary>
	/// Loads the obxTEmp
	/// </summary>
	/// <param name="">The references to OBX segment .</param>
	std::vector<std::string> loadOBXAlarm();

	
	/// <summary>
	/// Loads the obx alar sock.
	/// </summary>
	/// <returns></returns>
	std::vector<std::string> loadOBXAlarSock();
	
	
	/// <summary>
	/// Gets the obx. load header
	/// </summary>
	/// <returns></returns>
	HL7_24::OBX * getOBXHl(HL7_24::OBX *, const std::string  &, const std::string  &);
	
	/// <summary>
	/// Cleans the all string using in MSH.
	/// </summary>
	//void cleanMSH();
	
	/// <summary>
	/// Cleans the pid, segmente
	/// </summary>
	//void cleanPID();
	
	/// <summary>
	/// Cleans the pV2 CLEan the PV1
	/// </summary>
	//void cleanPV1();
	
	/// <summary>
	/// Clean the OBR
	/// </summary>
	//void cleanoOBR();
	
	/// <summary>
	/// Cleans the obx.
	/// </summary>
	//void cleanOBX();


	public:

		/// <summary>
		/// Setgs the ga.
		/// </summary>
		void setgGa(GestorArchivo *);

		/// <summary>
		/// Gets the ga.
		/// </summary>
		/// <returns></returns>
		GestorArchivo * getGa();


		/// <summary>
		/// Prevents a default instance of the <see cref="MapeadorHl7"/> class from being created.
		/// </summary>
		MapeadorHl7();

	
		/// <summary>
		/// Finalizes an instance of the <see cref="MapeadorHl7"/> class.
		/// </summary>
		virtual ~MapeadorHl7();

	
		/// <summary>
		/// Adds to message.
		/// </summary>
		/// <returns></returns>
		void addToMessage(const std::string &);

		
		/// <summary>
		/// Gets the alarm.
		/// </summary>
		/// <returns></returns>
		std::vector<std::string> getAlarm();


		
		
		/// <summary>
		/// Loads the MSH.
		/// </summary>
		std::string loadMSH();

		
		/// <summary>
		/// Loads the pid segment HL7
		/// </summary>
		std::string loadPID();
		
		/// <summary>
		/// Loads the pv1. segment of the HL7 mesasge
		/// </summary>
		std::string loadPV1();

		
		/// <summary>
		/// Loads the obrs. this is the (n) obr segment for the message
		/// </summary>
		std::string loadOBR();

		/// <summary>
		/// Loads the obrs. this is the (n) obr segment for the message
		/// </summary>
		std::vector<std::string> loadOBX();

		
		/// <summary>
		/// this method generate UUID versinon 4 variant 1 
		/// </summary>
		/// <returns></returns>
		std::string UUID4();
		
		/// <summary>
		/// Sets the date.
		/// </summary>
		/// <param name="">The .</param>
		void setDate(const std::string  &);
		
		/// <summary>
		/// Sets the date.
		/// </summary>
		/// <param name="">The .</param>
		std::string getDate();
		
		/// <summary>
		/// Cleans this instance.
		/// </summary>
		void clean();
		
		/// <summary>
		/// Loads the static var to file.
		/// </summary>
		void loadFile();
		 		
		/// <summary>
		/// Getpipes the out.
		/// </summary>
		/// <returns></returns>
		std::map<std::string, std::vector<std::string>> getpipeOUT();


		

};

#endif