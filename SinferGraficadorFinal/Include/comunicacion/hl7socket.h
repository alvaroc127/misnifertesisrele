/*
 * This file is part of Auriga HL7 library.
 *
 * Auriga HL7 library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Auriga HL7 library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Auriga HL7 library.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#if !defined (HL7SOCKET_H_)
#define HL7SOCKET_H_


#include <iostream>
#include <sys/types.h>
#include <windows.h>
#include <Winsock2.h>
#include <Ws2tcpip.h>
#include <common/HL7Exception.h>

#define closeso closesocket
#define MAX_INSIDE_BUFFER_LEN 1200





class  HL7Socket
{
	private:

	int _socketfd;

	protected:
	size_t write(int fd, const void *buf, size_t count);
	
	size_t read_soc(int fd, const void *buf, size_t count);
	
	int ioctl(int fd, unsigned long cmd, int* argp);

	void bzero(char* buffer, int max_len);
	


	public:
	HL7Socket(const std::string &, const std::string &);

	HL7Socket(int);

	HL7Socket();

	~HL7Socket();

	size_t send_msg(const std::string& data);

	size_t read_msg(std::string& data);
		
	 int getSocket();

	 void conectar(const std::string &, const std::string &);

	
//#ifdef TESTS
//    void setSocket( int new_sock ){ _socketfd = new_sock; }
//#endif // TESTS


};


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 * =====================================================================================
 */
/*
void socket_test()
{
	std::string hl7_message = "MSH|^~\\&|system1|W|system2|UHN|200105231927||ADT^A01^ADT_A01|22139243|P|2.4\t\
EVN|A01|200105231927|\t\
PID||9999999999^^|2216506^||Duck^Donald^^^MR.^MR.||19720227|M|||123 Foo ST.^^TORONTO^ON^M6G 3E6^CA^H^~123 Foo ST.^^TORONTO^ON^M6G 3E6^CA^M^|1811|( 416 )111 - 1111||E^ENGLISH|S|PATIENT DID NOT INDICATE|211004554^||||||||||||\t\
PV1|||ZFAST TRACK^WAITING^13|E^EMERGENCY||369^6^13^U EM EMERGENCY DEPARTMENT^ZFAST TRACK WAITING^FT WAIT 13^FTWAIT13^FT WAITING^FTWAIT13|^MOUSE^MICKEY^M^^DR.^MD|||SUR||||||||I|211004554^||||||||||||||||||||W|||||200105231927|||||\t\
PV2||F|^R / O APPENDICIAL ABSCESS|||||||||||||||||||||||||\t\
IN1|1||001001|OHIP||||||||||||^^^^^|||^^^^^^M^|||||||||||||||||||||||||^^^^^^M^|||||\t\
ACC|";

	std::string host = "192.168.150.8";
	std::string port = "6661";

	HL7Socket hs(host, port);
	hs.send_msg(hl7_message);

	//HL7SocketServer server(1234);
	//server.run()
}*/

#endif //HL7SOCKET_H_
