#if !defined(_CAPTURADERED_)
#define _CAPTURADERED_
#define TINS_STATIC
#pragma once
#include <vector>
#include <tins\tins.h>
#include "Trama.h"
#include "MindrayPacket.h"
#include "MindrayAlarma.h"
#include "MindrayParametros.h"
#include "SubTramaECG.h"
#include "SubTramaAlarma.h"
#include "SubTramaArt_AP.h"
#include "SubTramaImpedancia.h"
#include "SubtRamTemp.h"
#include "SubTramSpo2.h"
#include "GestorArchivo.h"
#include "StructDB.h"
#include "Coneccion.h"
#include "comunicacion/BeanComunicacion.h"
#include <log4z.h>
 




/// <summary>
/// Class CapturaDeRed this is a logical, representation for the procees signals
/// </summary>
class CapturaDeRed
{
	
private:
	Store alm;
	MindrayAlarma ma;
	MindrayPacket mp;
	MindrayParametros mpp;
	std::vector<uint8_t> datWait;
	std::vector<Tins::Packet> storePack;
	std::string dat_time;
	bool wait =true;
	int posG=0;
	GestorArchivo ga;
	Conection co;
	TIMESTAMP_STRUCT ts;
	BeanComunicacion beanC;
	HANDLE syncmutex;

public:
	/// <summary>
	/// Initializes a new instance of the <see cref="CapturaDeRed"/> class.
	/// </summary>
	CapturaDeRed();


	/// <summary>
	/// Finalizes an instance of the <see cref="CapturaDeRed"/> class.
	/// </summary>
	virtual ~CapturaDeRed();


	

	/// <summary>
	/// sort the packet of networck for save in the arch
	/// </summary>
	 std::string clasificarPacket(const  std::vector<uint8_t> & , int );
	
	/// <summary>
	/// Configurations the capture.
	/// </summary>
	/// <returns></returns>
	Tins::NetworkInterface  configCapture();
		
	/// <summary>
	/// Confingrations the sniffer.
	/// </summary>
	/// <returns></returns>
	Tins::SnifferConfiguration  configSniffer();
	
	/// <summary>
	/// Starts the capture.
	/// </summary>
	void startCapture();
		
	/// <summary>
	/// Capturars the packet1.
	/// </summary>
	void CapturarPacket1();

	
	/// <summary>
	/// Confs the head.
	/// </summary>
	/// <returns></returns>
	int confHead(const std::string  &head,const  std::string &ip,const std::vector<uint8_t>&, int);
	
	/// <summary>
	/// Capture the data time.
	/// </summary>
	/// <returns></returns>
	std::string  captDta_time();

	
	/// <summary>
	/// Caps the dta timefor HL7.
	/// </summary>
	/// <returns></returns>
	std::string capDta_timeforHl7();
		

		
	/// <summary>
	/// Sets the vector.
	/// </summary>
	/// <param name="">The .</param>
	void setVector(const std::vector<uint8_t> &);
	
	/// <summary>
	/// Gets the data wait.
	/// </summary>
	/// <returns></returns>
	std::vector<std::uint8_t>  getDataWait();
		
	/// <summary>
	/// Buscars the head.
	/// </summary>
	/// <param name="">The .</param>
	/// <param name="">The .</param>
	/// <returns></returns>
	int searchHead(const std::vector<uint8_t> &, int);
	
	/// <summary>
	/// Guardars the mp.
	/// </summary>
	bool guardarMP(MindrayPacket &);	

	/// <summary>
	/// Guardars the MPP.
	/// </summary>
	bool guardarMPP(MindrayParametros &);	

	/// <summary>
	/// Guardars the ma.
	/// </summary>
	bool guardarMA(MindrayAlarma &);
		
	/// <summary>
	/// Almacens the database.
	/// </summary>
	void almacenDB();
		
	/// <summary>
	/// Clasifis the data.
	/// </summary>
	void clasifiData();

};


#endif

